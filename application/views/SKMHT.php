<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Halaman Print A4</title>
</head>
<style type="text/css">
/* Kode CSS Untuk PAGE ini dibuat oleh http://jsfiddle.net/2wk6Q/1/ */
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .format{
      margin-left: 2.59cm;
    }
    .subpage {
        padding: 1cm;
        border: 5px black solid;
        height: 257mm;
        text-align: center;
    }

    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
        #footer{
    height:50px;
    line-height:50px;
    background:#333;
    color:#fff;
}
    }
</style>
<body>
	<?php
	function penyebut($nilai) {
			$nilai = abs($nilai);
			$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
			$temp = "";
			if ($nilai < 12) {
				$temp = " ". $huruf[$nilai];
			} else if ($nilai <20) {
				$temp = penyebut($nilai - 10). " belas";
			} else if ($nilai < 100) {
				$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
			} else if ($nilai < 200) {
				$temp = " seratus" . penyebut($nilai - 100);
			} else if ($nilai < 1000) {
				$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
			} else if ($nilai < 2000) {
				$temp = " seribu" . penyebut($nilai - 1000);
			} else if ($nilai < 1000000) {
				$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
			} else if ($nilai < 1000000000) {
				$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
			} else if ($nilai < 1000000000000) {
				$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
			} else if ($nilai < 1000000000000000) {
				$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
			}
			return $temp;
		}

		function terbilang($nilai) {
			if($nilai<0) {
				$hasil = "minus ". trim(penyebut($nilai));
			} else {
				$hasil = trim(penyebut($nilai));
			}
			return $hasil;
		}
?>
<div class="book">
    <div class="page">
        <div class="subpage">
          <div class="format">
						<?php foreach ($SKMHT->result() as $view) { ?>
						<p style="margin: 0cm 0cm 0.0001pt 160pt; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 36pt; line-height: 23pt; text-align: left;"><strong><span style="font-size:27px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;NOTARIS</span></strong></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; text-align: center; line-height: 23pt;"><strong><span style="font-size:27px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">NUR AINI MAULIDA,S.H.,M.Kn.</span></strong></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">DAERAH KERJA KABUPATEN BANYUWANGI</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">SK.MENTERI HUKUM DAN HAK ASASI MANUSIA&nbsp;</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">REPUBLIK INDONESIA</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Nomor AHU.418.AH.02.01 Tahun 2013, Tanggal 30-08-2013</span></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><span style="font-size: 18px; font-family: &quot;Bookman Old Style&quot;, serif;"><strong>________________________________________________</strong> </span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Jl.Brawijaya Nomor 18 Banyuwangi</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Telp./ Fax : &nbsp;0333 - 423411</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">SURAT KUASA&nbsp;</span></strong></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">MEMBEBANKAN HAK TANGGUNGAN</span></strong></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Nomor </span><?php echo $view->NOMOR_AKTA ?></span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><em><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Lembar Kedua&nbsp;</span></em></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:justify;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Pada hari ini, tanggal <?php echo date("d-n-Y",strtotime($view->TGL_AKTA)) ?> terbilang <?php date("d",strtotime($view->TGL_AKTA)) ?>.' '.date("F",strtotime($view->TGL_AKTA)).' '.terbilang (date("Y",strtotime($view->TGL_AKTA))).'). -------------------------------------</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:justify;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Menghadap kepada saya, <strong>NUR AINI MAULIDA, Sarjana Hukum,--- Magister Kenotariatan,</strong> yang berdasarkan Surat Keputusan---- Menteri Hukum dan Hak Asasi Manusia Republik Indonesia--- Nomor AHU.418.AH.02.01 Tahun 2013, Tanggal 30-08-2013,---- diangkat sebagai Notaris di Kabupaten Banyuwangi, dengan---- dihadiri oleh saksi-saksi yang nama-namanya akan disebutkan--- pada bagian akhir akta ini: ------------------------------------------------</span></p>
						<ol style="margin-bottom:0cm;list-style-type: upper-roman;">
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Nyonya</span><strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong><strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">'.$view->NAMA_COMPARATOR_CUSTOMER.'</span></strong><strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">,&nbsp;</span></strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Lahir di '.$view->TEMPAT_LAHIR.', tanggal ----------'.date('d-m-Y',strtotime($view->TANGGAL_LAHIR)).' ('.terbilang (date("d",strtotime($view->TANGGAL_LAHIR))).' '.date("F",strtotime($view->TANGGAL_LAHIR)).' '.terbilang (date("Y",strtotime($view->TANGGAL_LAHIR))).')&nbsp;</span>Warga Negara Indonesia, Karyawan---- BUMN, bertempat tinggal di '.$view->ALAMAT_COMPARATOR_CUSTOMER.', Rukun Tetangga '.$view->RT_COMPARATOR_CUSTOMER.', Rukun Warga '.$view->RW_COMPARATOR_CUSTOMER.', '.$view->DESA_KELURAHAN_COMPARATOR_CUSTOMER.', '.$view->KECAMATAN_COMPARATOR_CUSTOMER.', '.$view->KABUPATEN_COMPARATOR_CUSTOMER.'. Pemilik--- Kartu Tanda Penduduk dengan Nomor Induk Kependudukan---- '.$view->NO_KTP_COMPARATOR_CUSTOMER.'. '.substr($view->ISI_COMPARATOR_CUSTOMER, 404).' ----------
						    <ul></ol>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;margin-left:13.5pt;text-align:justify;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Penghadap tersebut diatas melakukan tindakan hukum dalam akta ini, telah mendapat persetujuan dari&nbsp;</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Istrinya yang sah</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">, yang turut pula hadir dihadapan saya,&nbsp;</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Notaris</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;dan menanda</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">-</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">tangani Akta ini</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">,&nbsp;</span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Nyonya <strong>'.$view->PASANGAN_DETAIL_ORDER.', </strong>'.$view->BIODATA_PASANGAN_DETAIL_ORDER.'</span></p>
						<p style="margin-top:0cm;margin-right:0cm;margin-bottom:.0001pt;margin-left:13.5pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:justify;line-height:22.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Pemegang hak atas tanah yang akan dijadikan Obyek Hak ------Tanggungan. Selanjutnya disebut <strong>PEMBERI KUASA</strong></span><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; -------------</span></p>
						<ol style="list-style-type: undefined;">
						  <li><strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">'.$view->DETAIL_ORDER_ATAS_NAMA.'&nbsp;</span></strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;'.$view->ISI_COMPARATOR_CUSTOMER.'.&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Bertindak</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">dalam&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">---------</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">kedudukannya</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">selaku&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">SUB BRANCH HEAD</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><strong><span style="font-family:&quot;Courier New&quot;;font-size:12.0pt;">'.$view->NAMA_CUSTOMER.'</span></strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">berdasarkan</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Surat</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Keputusan Nomor&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">13</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">/</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">SK/BWI.III/OPS/II/ 2018</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">, tanggal&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">21 F</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">ebruari 20</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">18,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;jo Akta&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Pernyataan Keputusan Rapat Umum Pemegang Saham Tahunan nomor 51,&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">tanggal&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">24 Maret</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;20</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">15</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">, dibuat dihadapan <strong>FATHIAH</strong></span><strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">-</span></strong><strong><span style="font-family:&quot;Courier New&quot;;font-size:12.0pt;">&nbsp;HELMI, Sarjana Hukum</span></strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;Notaris d</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">i</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;Jakarta,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;yang pemberitahuannya telah diterima oleh Menteri Hukum dan Hak Asasi Manusia Republik Indonesia berdasarkan Surat Nomor AHU-AH.01.03-0926094,--tanggal 21 April 2015 junco Akta Pernyataan Keputusan Rapat Umum Pemegang Saham Tahunan &nbsp;nomor 40, tanggal 12 April 2016, yang pemberitahuannya telah diterima oleh Menteri Hukum dan Hak Asasi Manusia Republik Indonesia berdasar-kan Surat Nomor</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">AHU-AH.01.03-0039336, tanggal 12 April 2016&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">dengan demikian sah bertindak untuk</span><span style="font-family:&quot;Courier New&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">dan</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">atas</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">nama <strong>'.$view->NAMA_CUSTOMER.'</strong></span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">.</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">--------------------</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span>
						    <ul>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Selanjutnya disebut <strong>PENERIMA KUASA</strong> -----------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Penghadap dikenal oleh saya, Notaris. --------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Pemberi Kuasa menerangkan dengan ini memberi kuasa kepada Penerima Kuasa. ----------------------------------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Untuk membebankan Hak Tanggungan Ke I (pertama) guna menjamin pelunasan hutang Tuan <strong>,&nbsp;</strong>tersebut.- ---</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Selaku Debitur sejumlah Rp. <strong><span style="color:black;">'.number_format($view->NOMINAL_DETAIL_ORDER).',- ('.terbilang($view->NOMINAL_DETAIL_ORDER).' rupiah),&nbsp;</span></strong><span style="color:black;">atau</span><strong>&nbsp;</strong>sejumlah uang yang dapat ditentukan------- dikemudian hari berdasarkan perjanjian utang piutang yang----- ditandatangani oleh Debitur/Pemberi Kuasa dengan: -------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Perseroan Terbatas <strong>PT. BANK TABUNGAN NEGARA (Persero) Tbk,</strong> yang berkedudukan dan berkantor pusat di Jakarta--------- melalui cabangnya di Banyuwangi. ------------------------------------selaku Kreditur dibuktikan dengan : ----------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;color:black;font-size:12.0pt;color:black;">Surat Perjanjian Kredit antara Perseroan Terbatas <strong>PT. BANK-----&nbsp;</strong></span><strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">TABUNGAN NEGARA (Persero) Tbk dan HARIYANTO</span></strong><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;Nomor---- <span style="color:white;">0022420180730000008 yang dibuat dibawah tangan bermaterai&nbsp;</span>cukup yang dilegalisasi oleh saya, Notaris tertanggal&nbsp;</span><span style="font-size:16px;">'.date("d F Y",strtotime($view->TANGGAL_OBYEK)).'</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;------ dibawah Nomor 04/L/NA/VIII/2019, yang surat asli/salinan ----resminya diperlihatkan kepada saya dan penambahan, -----perubahan, perpanjangan serta pembaharuannya yang mungkin diadakan sampai sejumlah Nilai <span style="color:black;">tanggungan&nbsp;</span>sebesar Rp. '.number_format($view->NOMINAL_PARTIAL_OBYEK).',- ('.terbilang($view->NOMINAL_PARTIAL_OBYEK).' rupiah)<span style="color:white;">)</span><span style="color:black;">&nbsp;atas obyek Hak Tanggungan berupa 1 (satu) bidang &nbsp;Hak Atas Tanah/Hak Milik&nbsp;</span>Satuan Rumah Susun yang diuraikan dibawah ini : --------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Hak Guna Bangunan Nomor :&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">00120/Sukomaju,atas</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;sebidang tanah sebagaimana diuraikan dalam Surat Ukur tanggal '.date("d F Y",strtotime($view->TANGGAL_IDENTIFIKASI_OBYEK)).'</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">, Nomor: 00342/Sukomaju/2018,seluas '.$view->LUAS_OBYEK.' M<sup>2&nbsp;</sup> (delapan puluh empat</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;meter persegi</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">) dengan Nomor Identifi-kasi Bidang Tanah (NIB): '.$view->NOMOR_IDENTIFIKASI_OBYEK.' dan----- Pemberitahuan Pajak Terhutang Pajak Bumi dan Bangunan--- (SPPTPBB)&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Nomor</span><span style="font-size:16px;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Objek</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Pajak</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">(NOP)</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">: '.$view->NOMOR_OBYEK_PAJAK.'-------------------------------------------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Propinsi&nbsp; &nbsp; &nbsp; &nbsp;: Jawa Timur; --------------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Kabupaten&nbsp; &nbsp;: Banyuwangi; --------------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Kecamatan&nbsp; &nbsp;: Srono; ----------------------------------------------</span></li>
						      <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Desa&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: Sukomaju; ----------------------------------------</span></li>
						    </ul>
						  </li>
						</ol>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;margin-left:9.0pt;text-align:justify;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Akta Jual Beli tertanggal 19 ( Sembilan belas ) Agustus 2019(dua ribu sembilan belas) Nomor: 210/2019 yang dibuat dihadapan NUR AINI MAULIDA, Sarjana Hukum, Magister Kenotariatan.- ---</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:justify;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Selaku Pejabat Pembuat &nbsp;Akta tanah Kabupaten Banyuwangi. -----</span></p>
						<ul>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Berdasarkan alat-alat bukti berupa : ---------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Sertipikat Tanda Bukti Hak Guna Bangunan Nomor 00120/Desa Sukomaju dengan Nomor Seri AAJ971087 tertulis atas nama <strong>PT. MALINDO DUA</strong>. ------------------------------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Sertipikat dan bukti pemilikan yang disebutkan di atas diperlihat kan kepada saya, Notaris untuk keperluan pembuatan Surat----- Kuasa Membebankan Hak Tanggungan ini : --------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Obyek Hak Tanggungan ini meliputi pula : ---------------------------</span></li>
						</ul>
						<p style="margin-top:0cm;margin-right:0cm;margin-bottom:.0001pt;margin-left:9.0pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:justify;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Segala sesuatu yang berdiri, tertanam dan ditempatkan--------- diatasnya yang karena jenis dan ketentuannya menurut hukum -dapat dianggap sebagai benda tetap yang menurut keterangan-nya seluruhnya milik Tuan <strong>HARIYANTO</strong>. --------------------------</span></p>
						<ul>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Kuasa untuk membebankan Hak Tanggungan ini meliputi kuasa untuk menghadap dimana perlu, memberikan keterangan---------keterangan serta memperlihatkan dan menyerahkan surat-surat yang diminta, membuat/minta dibuatkan serta menandatangani &nbsp;Akta Pemberian Hak Tanggungan serta surat-surat lain yang -----diperlukan, memilih domisili, memberi pernyataan bahwa obyek- Hak Tanggungan betul milik Pemberi Kuasa, tidak tersangkut---- dalam sengketa, bebas dari sitaan dan beban-beban apapun,----- mendaftarkan Hak Tanggungan tersebut, memberikan dan-------- menyetujui syarat-syarat atau aturan-aturan serta janji-janji----- yang disetujui oleh Pemberi Kuasa dalam Akta Pemberian Hak--- Tanggungan tersebut, sebagai berikut : -------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji yang membatasi kewenangan pemberi Hak Tanggungan----- untuk menyewakan obyek Hak Tanggungan dan/atau menen-tukan atau mengubah jangka waktu sewa dan/atau menerima --sewa di muka, kecuali dengan persetujuan tertulis lebih dahulu-- dari pemegang Hak Tanggungan; --------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji yang membatasi kewenangan pemberi Hak Tangungan----- untuk mengubah bentuk atautata susunan obyek Hak--------- Tanggungan kecuali dengan persetujuan tertulis lebih dahulu---- dari pemegang Hak Tanggungan; ---------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji yang memberikan kewenangan kepada pemegang Hak------ Tanggungan untuk menyelamatkan obyek Hak Tanggungan, jika- hal itu diperlukan untuk pelaksanaan eksekusi atau untuk------- mencegah menjadi hapusnya atau dibatalkannya hak yang----- menjadi obyek Hak Tanggungan karena tidak dipenuhinya atau-- dilanggarnya ketentuan undang-undang serta kewenangan------- untuk mengajukan permohonan memperpanjang jangka waktu-- dan/atau memperbarui hak atas tanah yang menjadi obyek Hak- Tanggungan. ----------------------------------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji bahwa pemegang Hak Tanggungan pertama mempunyai ----hak untuk menjual atas kekuasaan sendiri obyek Hak Tanggung-an apabila debitur cidera janji; ------------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji yang diberikan oleh pemegang Hak Tanggungan pertama--- bahwa obyek Hak Tanggungan tidak akan dibersihkan dari Hak-- Tanggungan; ----------------------------------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji bahwa pemberi Hak Tanggungan tidak akan melepaskan---- haknya atas obyek Hak Tanggungan tanpa persetujuan tertulis--- lebih dahulu dari pemegang Hak Tanggungan; -----------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji bahwa pemegang Hak Tanggungan akan memperoleh------ seluruh atau sebagian dari ganti rugi yang diterima pemberi Hak Tanggungan untuk pelunasan piutangnya apabila obyek Hak----- Tanggungan dilepaskan haknya oleh pemberi Hak Tanggungan--- atau dicabut haknya untuk kepentingan umum; -------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji bahwa pemegang Hak Tanggungan akan memperoleh------- seluruh atau sebagian dari uang asuransi yang diterima pemberi Hak Tanggungan untuk pelunasan piutangnya, jika obyek Hak--- Tanggungan diasuransikan; ---------------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji bahwa pemberi Hak Tanggungan akan mengosongkan------ obyek Hak Tanggungan pada waktu eksekusi Hak Tanggungan; -</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Janji bahwa Sertipikat Hak atas tanah yang telah dibubuhi -----catatan pembebanan Hak Tanggungan diserahkan kepada dan--- untuk disimpan Pemegang Hak Tanggungan; ------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Dan untuk pelaksanaan janji-janji tersebut memberikan kuasa---yang diperlukan kepada Pemegang Hak Tanggungan di dalam Akta Pemberian Hak Tanggungan; --------------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Kuasa yang diberikan dengan akta ini tidak dapat ditarik------- kembali dan tidak berakhir karena sebab apapun kecuali oleh---- karena telah dilaksanakan pembuatan Akta Pemberian Hak------ Tanggungan selambat-lambatnya atau sampai dengan perjanjian pokoknya berakhir dan/atau kreditnya dinyatakan lunas -------dan/atau menjadi lunas serta pendaftarannya atau karena -------tanggal tersebut telah terlampaui tanpa dilaksanakan pembuat-an Akta Pemberian Hak Tanggungan. ----------------------------------</span></li>
						  <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Demikian akta ini dibuat dihadapan para pihak dan : --------------</span>
						    <ol style="list-style-type: decimal;">
						      <li><span style="font-size:16px;">Nyonya <strong>'.$view->SAKSI_SATU.',</strong> '.$view->BIODATA_SAKSI_SATU.' -----------------------------------</span></li>
						      <li><span style="font-size:16px;">Nyonya <strong>'.$view->SAKSI_DUA.',</strong> '.$view->BIODATA_SAKSI_DUA.' -----------------------------------</span></li>
						        <ul>
						          <li><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Sebagai saksi-saksi, dan setelah dibacakan serta dijelaskan, ---maka sebagai bukti kebenaran pernyataan yang dikemukakan- &nbsp;oleh Pemberi Kuasa dan Penerima Kuasa tersebut diatas, akta--ini ditanda tangani oleh Pemberi Kuasa, Penerima Kuasa, para saksi dan Saya,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">Notaris</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">,</span><span style="font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;font-size:12.0pt;">&nbsp;sebanyak 2 (dua) rangkap asli terdiri &nbsp;dari 1 (satu) rangkap lembar pertama disimpan di kantor Saya, dan 1 (satu) rangkap lembar kedua disampaikan kepada ----- Penerima Kuasa untuk dipergunakan sebagai dasar penanda-tanganan Akta Pemberian Hak Tanggungan yang -------------bersangkutan. ------------------------------------------------------------</span></li>
						           <p style="text-align: center; margin-right:5%"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Pemberi Kuasa &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Penerima Kuasa</span></p>
						        </ul>
						      </li>
						    </ol>
						  </li>
						</ul>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;margin-right:-26.15pt;line-height:23.0pt;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;margin-right:-26.15pt;line-height:23.0pt;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt 1cm; font-size: 15px; font-family: Calibri, sans-serif; text-indent: -63pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></strong><strong><u><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">HARIYANTO</span></u></strong><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></strong><strong><u><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">'.$view->DETAIL_ORDER_ATAS_NAMA.'</span></u></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt 0cm; font-size: 15px; font-family: Calibri, sans-serif; text-indent: -63pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; QQ, '.$view->NAMA_CUSTOMER.'&nbsp;</span></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt -7cm; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 27pt; line-height: 23pt; text-align: center;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">Persetujuan Istri</span></p>
						<p style="margin: 0cm -26.15pt 0.0001pt 180pt; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 36pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt 180pt; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 36pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt 180pt; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 36pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt -8cm; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 27pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;<u>'.$view->PASANGAN_DETAIL_ORDER.'</u></span></strong></p>
						<p style="margin: 0cm -26.15pt 0.0001pt 180pt; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 36pt; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;color:red;">&nbsp;</span></strong></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Saksi &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Saksi</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="color:white;">ttd&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; ttd</span></span></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></p>
						<p style="margin: 0cm 0cm 0.0001pt 2cm; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp;&nbsp;</span></strong><strong><u><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">'.$view->SAKSI_SATU.'&nbsp;</span></u></strong><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;<u>'.$view->SAKSI_DUA.'</u></span></strong></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin: 0cm 0cm 0.0001pt; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Notaris Kabupaten Banyuwangi</span></p>
						<p style="margin:0cm;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;text-align:center;line-height:23.0pt;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></strong></p>
						<p style="margin: 0cm 0.85pt 0.0001pt 0cm; font-size: 15px; font-family: Calibri, sans-serif; text-align: center; line-height: 23pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="color:white;">ttd</span></span></p>
						<p style="margin: 0cm 0.85pt 0.0001pt 0cm; font-size: 15px; font-family: Calibri, sans-serif; text-align: center; line-height: 23pt;"><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp;</span></p>
						<p style="margin: 0cm 0.85pt 0.0001pt 18pt; font-size: 15px; font-family: Calibri, sans-serif; text-indent: -18pt; line-height: 23pt; text-align: center;">
						  <br>
						</p>
						<p style="margin: 0cm 0cm 0.0001pt 2cm; font-size: 15px; font-family: Calibri, sans-serif; line-height: 23pt; text-align: center;"><strong><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">&nbsp; &nbsp;&nbsp;</span></strong><strong><u><span style="font-size:16px;font-family:&quot;Bookman Old Style&quot;,&quot;serif&quot;;">NUR AINI MAULIDA, S.H., M.Kn.</span></u></strong></p>
						  <br>
						</p>
						<p>
						  <br>
						</p>
					<?php	} ?>
          </div>
        </div>
    </div>

</div>
</body>
</html>
<script type="text/javascript">window.print();</script>
