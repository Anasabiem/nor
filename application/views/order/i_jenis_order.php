<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Tambah Jenis Order</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
			<li class="active">Tambah Jenis Order</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Tambah Jenis Order Baru</h3>
			</div>
			<div class="box-body">
				<form role="form" method="POST" action="<?php echo base_url('Jenis_order/tambahJenisOrder') ?>">
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Jenis Order</label>
							<input type="text" class="form-control" required="" placeholder="Jenis Order" name="jns_order">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Obyek Untuk Order</label>
							<input type="text" class="form-control" required="" placeholder="Jenis Obyek yang digunakan untuk order" name="oby_order">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Asal Obyek</label>
							<input type="text" class="form-control" required="" placeholder="Asal obyek, apakah terdapat peningkatan atau perubahan status obyek untuk order" name="asl_obyek">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Keterangan</label>
							<input type="text" class="form-control" required="" placeholder="Keterangan untuk jenis order" name="ket">
						</div>
					</div>
					<div class="box-footer">
						<button style="float: right;" type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
