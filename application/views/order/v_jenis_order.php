<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Jenis Order</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
			<li class="active">Jenis Order</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Jenis Order</h3>
			</div>
			<div class="box-body">
			<?php if ($this->session->userdata('tipene') == 4) { ?>
				<a class="btn btn-primary" href="<?php echo base_url('Jenis_order/tambah') ?>"><i class="fa fa-plus"> Tambah Jenis Order </i></a>
				<hr>
			<?php } ?>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Jenis Order</th>
							<th>Obyek Order</th>
							<th>Asal Obyek</th>
							<th>Keterangan</th>
							<?php if ($this->session->userdata('tipene') == 4) { ?>
								<th>Action</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($ord as $menu): ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?php echo $menu->NAMA_JENIS_ORDER ?></td>
							<td><?php echo $menu->OBYEK_JENIS_ORDER ?></td>
							<td><a href="<?php echo base_url('Jenis_order/proses_order/'.$menu->NO_ID_JENISORDER) ?>" title="edit" ><i class="fa fa-book"><?php echo $menu->ASAL_OBYEK_JENIS_ORDER ?></i></a></td>
							<td><?php echo $menu->KETERANGAN_JENIS_ORDER ?></td>
							<?php if ($this->session->userdata('tipene') == 4) { ?>
							<td>
							<a title="Ubah Data" href="<?php echo base_url('Jenis_order/editJenisOrder/'.$menu->NO_ID_JENISORDER) ?>"><i class="fa fa-edit"></i></a>
								<a href="<?php echo base_url('Jenis_order/hapusJenisOrder/'.$menu-> NO_ID_JENISORDER) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" ><i class="fa fa-trash" title="Hapus" style="color: red"></i></a>
							</td>
							<?php } ?>
						</tr>

					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
