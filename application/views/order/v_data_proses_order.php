<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Data Proses Order</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
			<li class="active">Data Proses Order</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Proses Order</h3>
			</div>
			<div class="box-body">
				<?php if ($this->session->userdata('idtipene') == 4) { ?>
					<a class="btn btn-primary" href="<?php echo base_url('Jenis_order/tambah_Proses_order/').$id_jenis; ?>" title="Tambah Data"><i class="fa fa-plus"> Tambah Proses Order </i></a>
				<hr>
				<?php } ?>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Proses Order</th>
							<th>Posisi Proses Order</th>
							<th>Keterangan</th>
							<?php if ($this->session->userdata('tipene') == 4) { ?>
							<th>Action</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($pros_ord as $menu): ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?php echo $menu->NAMA_PROSES_ORDER ?></td>
							<td><?php echo $menu->POST_PROSES_ORDER ?></td>
							<td><?php echo $menu->KETERANGAN_PROSES_ORDER ?></td>
							<?php if ($this->session->userdata('tipene') == 4) { ?>
							<td>
							<a href="<?php echo base_url('Jenis_order/editProsesOrder/').$menu->NO_ID_PROSES_ORDER ?>"><i class="fa fa-edit"></i></a>
								<a href="<?php echo base_url('Jenis_order/hapusProsesOrder/'.$menu->NO_ID_PROSES_ORDER) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-social-icon"><i class="fa fa-trash" title="Hapus" style="color: red"></i></a>
							</td>
							<?php } ?>
						</tr>

					<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
