<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Tambah Proses Order</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
			<li class="active">Tambah Proses Order</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Tambah Proses Order Baru</h3>
			</div>
			<div class="box-body">
				<form role="form" method="POST" action="<?php echo base_url('Jenis_order/prosesTambahOrder') ?>">
					<?php foreach ($jns_ord as $j): ?>
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Jenis Order</label>
							<input type="text" class="form-control" name="jns_order" value="<?php echo $j->NO_ID_JENISORDER; ?>" readonly="">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Keterangan Order</label>
							<input type="text" class="form-control" name="ket_order" value="<?php echo $j->KETERANGAN_JENIS_ORDER; ?>" readonly="">
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Nama Proses Order</label>
							<input type="text" class="form-control" required="" placeholder="Nama proses untuk order " name="nm_proses_order">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Posisi Proses Order</label>
							<input type="text" class="form-control" required="" placeholder="Posisi proses untuk order" name="pos_proses_order">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Keterangan Proses Order</label>
							<input type="text" class="form-control" required="" placeholder="Keterangan posisi proses untuk order" name="ket">
						</div>
					</div>
					<div class="box-footer">
						<button style="float: right;" type="submit" class="btn btn-primary">Simpan</button>
					</div>
					<?php endforeach; ?>
				</form>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
