<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    Home</a>
            </li>
            <li class="active">TUGAS</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Pembagian Tugas</h3>
            </div>
            <div class="box-body">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default1">
                    <i class="fa fa-plus">Tambah Pembagian</i>
                </button>

                <div class="modal fade" id="modal-default1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Tambah Pembagian</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" method="POST" action="<?php echo base_url('Bagi_menu/save_cetak') ?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Nama Karyawan</label>
                                            <div class="col-sm-10">
                                                <select name="nm_karyawan" id="" class="form-control">
                                                    <?php foreach ($user as $karyawan ) { ?>
                                                    <option value="<?= $karyawan->ID_USER ?>"><?= $karyawan->NAMA_USER ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Pembagian</label>
                                            <div class="col-sm-10">
                                                <?php foreach ($cust as $customer) { ?>
                                                <input name="Pembagian_cetak[]" type="checkbox" value="<?= $customer->NO_ID_CUSTOMER ?>"> <?= $customer->NAMA_CUSTOMER ?>
                                                <br>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <hr>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Karyawan</th>
                            <th>Cust ditangani</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($pembagian as $bagi): ?>
                            <?php $ambil= $this->M_model->get_pembagian_cust($bagi->NO_ID_USR )->result(); ?>
                        <tr>
                            <td><?= $no++?></td>
                            <td><?= $bagi->NAMA_USER ?></td>
                            <td>
                                
                                <?php foreach ($ambil as $cust): ?>
                                <small><?= $cust->NAMA_CUSTOMER ?>, </small> 
                                <?php endforeach ?>
                            </td>
                            <td>
                                <a href="" class="btn btn-link btn-just-icon remove" title="Ubah"> <i class="fa fa-pencil"></i> </a>
                                <a
                                    href=""
                                    title="hapus"
                                    onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')"
                                    class="btn btn-link btn-just-icon remove">
                                    <i class="fa fa-trash" style="color:red"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
