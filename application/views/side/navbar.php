<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="index2.html" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>I</b>n</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>I</b>-Norsys</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<?php $profil = $this->db->get_where('0_user',array('ID_USER'=>$this->session->userdata('id')))->result(); ?>
							<?php foreach ($profil as $key): ?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								
								<?php if ($key->PICT_USER == NULL) { ?>
									<img src="<?php echo base_url() ?>master/dist/img/avatar5.png" class="user-image" alt="User Image">
								 <?php } else { ?>
									<img src="<?php echo base_url() ?>gallery/photo_profile/<?= $key->PICT_USER ?>" class="user-image" alt="User Image">
								<?php }?>
									<span class="hidden-xs"><?php echo $key->NAMA_USER ?></span>
								</a>
								<ul class="dropdown-menu">
									<li class="user-footer">
										<div class="">
											<a href="<?php echo base_url('Home/profile/'.$key->ID_USER); ?>" class="btn btn-default btn-flat">Pengaturan Akun</a>
											<a href="<?php echo base_url('LoginUser/logout'); ?>" class="btn btn-default btn-flat">Keluar</a>
										</div>
									</li>
								</ul>
							<?php endforeach; ?>
						</li>
						<!-- Control Sidebar Toggle Button -->
						<li>
							<a href="#" data-toggle="control-sidebar"></a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MAIN NAVIGATION</li>
					<?php $menu = $this->M_model->ambyar()->result() ?>
					<?php foreach ($menu as $ambyar): ?>
						<?php $sub = $this->M_model->anakan($ambyar->id) ?>
						<?php if ($sub->num_rows()==NULL): ?>
							<li class="<?php if ($this->uri->segment('1')==$ambyar->url): ?>
								active
							<?php endif ?>">
							<a href="<?php echo base_url($ambyar->url) ?>">
							<?php else: ?>
								<li class="treeview">
									<a href="#">
									<?php endif; ?>
									<i class="fa <?php echo $ambyar->icon ?>"></i>
									<span><?php echo $ambyar->title ?></span>
									<?php if (!$sub->num_rows()==NULL): ?>
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									<?php endif; ?>
								</a>

								<ul class="treeview-menu">
									<?php foreach ($sub->result() as $key): ?>
										<li><a href="<?php echo base_url().$key->url ?>"><i class="fa <?php echo $key->icon ?>"></i> <?php echo $key->title ?></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
						<?php endforeach; ?>
						<li class="header">Fast Acces</li>
						<li>
							<a href="<?= base_url('Home/Profile') ?>"> <i class="fa fa-user"></i> PROFILE</a>
						</li>
						<li>
							<a href="<?php echo base_url('LoginUser/logout'); ?>"> <i class="fa fa-power-off"></i> LOGOUT</a>
						</li>
					</ul>
				</section>
				<!-- /.sidebar -->
			</aside>
