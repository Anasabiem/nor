<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User Profile
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Examples</a></li>
      <li class="active">User profile</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <?php foreach ($usr->result() as $key): ?>
            <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>gallery/photo_profile/<?php echo $key->PICT_USER; ?>" alt="User profile picture">
            <h3 class="profile-username text-center"><?php echo $key->NAMA_USER ?></h3>

            <small>Ganti Foto</small>
            <form class="form" action="<?php echo base_url('Home/upload'); ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" name="ID_USER" value="<?php echo $key->ID_USER; ?>">
              <input type="hidden" name="PICT_USER" value="<?php echo $key->PICT_USER; ?>">
              <input type="file" name="userfile" />
              <input type="submit" value="upload" />
            </form>


            <!-- <p class="text-muted text-center">Software Engineer</p> -->
            <?php endforeach; ?>
            <ul class="list-group list-group-unbordered">

            </ul>

            <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Status</h3>
          </div>
          <div class="box-body">
            <strong>Aktif</strong>

            <!-- <p class="text-muted">
              B.S. in Computer Science from the University of Tennessee at Knoxville
            </p> -->
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab">Profile</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="activity">
              <!-- Post -->
              <div class="post">
                <form class="" action="<?php echo base_url('Home/perbarui') ?>" method="post">
                <?php foreach ($usr->result() as $key): ?>

                <div class="form-group">
                  <label >Username</label>
                  <input type="text" class="form-control" value="<?php echo $key->LOGIN_USER ?>"  name="LOGIN_USER" required="" >
                </div>
                <div class="form-group">
                  <label >Nama</label>
                  <input type="text" class="form-control" value="<?php echo $key->NAMA_USER ?>"  name="NAMA_USER" required="" >
                </div>
                <div class="form-group">
                  <label >Alamat</label>
                  <input type="text" class="form-control" value="<?php echo $key->ADDRS_USER ?>"  name="ADDRS_USER" required="" >
                </div>
                <div class="form-group">
                  <label >No Telpon</label>
                  <input type="text" class="form-control" value="<?php echo $key->PHONE_USER ?>"  name="PHONE_USER" required="" >
                </div>
                <div class="">
                  <button type="submit" onclick="javascript: return confirm('Anda yakin ingin memperbarui data?')" name="button" >Perbarui</button>
                </div>
              <?php endforeach; ?>
            </form>
              </div>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>

      <div class="col-md-9">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab">Ganti Password</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="activity">
              <!-- Post -->
              <div class="post">
                <form class="" action="<?php echo base_url('Home/perbarui_password') ?>" method="post">
                  <?php echo $error_password; ?>
                <div class="form-group">
                  <label >Password Baru</label>
                  <input type="password" class="form-control"  name="password_lama" value="">
                </div>
                <div class="form-group">
                  <label >Masukkan Kembali Password Baru</label>
                  <input type="password" class="form-control"  name="password_baru" value="">
                </div>
                <div class="">
                  <button type="submit" onclick="javascript: return confirm('Anda yakin ingin memperbarui password?')" name="button" >Perbarui</button>
                </div>
            </form>
              </div>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
