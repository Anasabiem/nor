<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">User	</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<button class="btn btn-primary" data-toggle="modal" data-target="#modal-default"> <i class="fa fa-plus"> Tambah User </i></button>
				<div class="modal fade" id="modal-default">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Tambah User</h4>
								</div>
								<div class="modal-body">
									<form class="form-horizontal" method="POST" action="<?php echo base_url('Menu_user/insertNU') ?>">
										<div class="box-body">
											<div class="form-group">
												<label >Nama User</label>
												<input type="text" class="form-control"  name="nmUser" required="">
											</div>
											<div class="form-group">
												<label >Login User</label>
												<input type="text" class="form-control"  name="lgnUser" required="">
											</div>
											<div class="form-group">
												<label>Password User</label>
												<input type="password" class="form-control"  name="psw" required="">
											</div>
											<div class="form-group">
												<label >Alamat</label>
												<input type="text" class="form-control"  name="alamat" required="">
											</div>
											<div class="form-group">
												<label >Nomor Telp</label>
												<input type="text" class="form-control"  name="telp" required="">
											</div>
											<?php if ($this->session->userdata('tipene')==1): ?>
												<div class="form-group">
													<label >Tipe Karyawan</label>
													<select class="form-control" name="type">
														<option value="4">User</option>
													</select>
												</div>
											<?php endif; ?>
											<?php if ($this->session->userdata('tipene')==4): ?>
												<div class="form-group">
													<label >Tipe Karyawan</label>
													<select class="form-control" name="type" >
														<option value="3">Inputer</option>
														<option value="2">Pencetak</option>
														<option value="5">Admin BPN</option>
													</select>
													<!-- <input class="form-control" type="hidden" name="type" value="2"> -->
												</div>
											<?php endif; ?>

											<div class="form-group">
												<label >Gambar User</label>
												<input class="form-control" type="file" name="gambar">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Simpan </button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<td>No</td>
								<td>NAMA USER</td>
								<td>LOGIN USER</td>
								<td>STATUS LOGIN</strong></td>
								<td>BSCSLR_USER</td>
								<td>NOMOR TELEPHONE</td>
								<td>STATUS KARYAWAN</td>
								<td>ACTION</td>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; foreach ($user->result() as $menu): ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $menu->NAMA_USER ?></td>
								<td><?php echo $menu->LOGIN_USER ?></td>
								<td><?php if ($menu->STATUS_LOGIN == 1): ?>
								<?php echo "AKTIF" ?>
								<?php else: ?>
									<?php echo "TIDAK AKTIF" ?>
									<?php endif ?></td>
									<!-- <td><?php echo $menu->ADDRS_USER  ?></td> -->
									<td><?php echo $menu->BSCSLR_USER  ?></td>
									<td><?php echo $menu->PHONE_USER  ?></td>
									<!-- <td><?php echo $menu->PICT_USER  ?></td> -->
									<td><?php echo $menu->STATUS_USER ?></td>
									<td><a type="button" class="btn btn-icon" data-toggle="modal" data-target="#modal-defaultq<?php echo $menu->ID_USER ?>"><i class="fa fa-pencil" title="Ubah"></i></a>

										<a type="button" href="<?php echo base_url('Menu_user/hapus/'.$menu->ID_USER); ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-icon"><i class="fa fa-trash" style="color: red" title="Hapus"></i></a>

										<a href="<?php echo base_url('Menu_user/inputMenuUser/'.$menu->ID_USER); ?>" type="button" class="btn btn-icon"><i class="fa fa-list" style="color:orange " title="List Menu"></i></a>

									</td>
									<div class="modal fade" id="modal-defaultq<?php echo $menu->ID_USER; ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title">Edit Menu </h4>
													</div>
													<div class="modal-body">
														<form class="form-horizontal" method="POST" action="<?php echo base_url('Menu_user/update') ?>">
															<div class="box-body">
																<div class="form-group">
																	<label >Nama User</label>
																	<input type="hidden" name="idNe" value="<?php echo $menu->ID_USER ?>">
																	<input type="text" class="form-control"  name="nmUser" required="" value="<?php echo $menu->NAMA_USER ?>">
																</div>
																<div class="form-group">
																	<label >Login User</label>
																	<input type="text" class="form-control"  name="lgnUser" required="" value="<?php echo $menu->LOGIN_USER ?>">
																</div>
																<div class="form-group">
																	<label>Password User</label>
																	<input type="password" class="form-control"  name="psw" required="" >
																</div>
																<div class="form-group">
																	<label >Alamat</label>
																	<input type="text" class="form-control"  name="alamat" required="" value="<?php echo $menu->ADDRS_USER ?>">
																</div>
																<div class="form-group">
																	<label >Nomor Telp</label>
																	<input type="text" class="form-control"  name="telp" required="" value="<?php echo $menu->PHONE_USER ?>">
																</div>
																<div class="form-group">
																	<label >Gambar User</label>
																	<input type="file" name="gambar">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
																<button type="submit" class="btn btn-primary">Save changes</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>

									</tr>

								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>
		<?php $this->load->view('side/footer') ?>

		<?php $this->load->view('side/js') ?>
