<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Customer
			<small>Data Customer</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Penentuan Harga Proses Customer</h3>
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Customer</th>
							<th>No Surat Order</th>
							<th>Customer Atas Nama</th>
							<th>gh</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($cus as $menu): ?>
						
							<tr>
								<td><?= $no++ ?></td>
							<td><?= $menu->NAMA_CUSTOMER ?></td>
								<td><?= $menu->NOMOR_SURAT_ORDER_CUSTOMER ?></td>
								<td><?= $menu->DETAIL_ORDER_ATAS_NAMA ?></td>
								<td></td>
								<td style="text-align: center;"> <a href=""> <i class="fa fa-list"></i> </a> </td>
						</tr>
						
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('side/footer') ?>

<?php $this->load->view('side/js') ?>