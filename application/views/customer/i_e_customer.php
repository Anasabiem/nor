<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Detail Customer
			<small>Tambah</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Tambah Customer</li>
		</ol>
	</section>
	<section class="content">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Tambah Data Detail Customer <?php foreach ($pemilik->result() as $key): ?>
						<?php echo $key->NAMA_CUSTOMER ?>
					<?php endforeach ?></h3>
				</div>
				<form role="form" method="POST" action="<?php echo base_url('Customer/tambahDetCus') ?>">
					<div class="box-body">
						<input type="hidden" name="idCus" value="<?php echo $key->NO_ID_CUSTOMER ?>">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Pemimpin</label>
							<input type="text" class="form-control" required="" placeholder="" name="nm_pem">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Penerima</label>
							<input type="text" class="form-control" required="" placeholder="" name="nm_penerima">
							<small style="color: red">untuk SKMHT</small>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Pemberi</label>
							<input type="text" class="form-control" required="" placeholder="" name="nm_pemberi">
							<small style="color: red">untuk SKMHT</small>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Istri Pemberi</label>
							<input type="text" class="form-control" required="" placeholder="" name="istri_pemberi">
							<small style="color: red">untuk SKMHT</small>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Saksi 1</label>
							<input type="text" class="form-control" required="" placeholder="" name="saksi1">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Saksi 2</label>
							<input type="text" class="form-control" required="" placeholder="" name="saksi2">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">No Ktp</label>
							<input type="text" class="form-control" name="ktp" required="" placeholder="">
						</div>
						<label>TTL</label>
						<div class="form-group row">	

							<div class=" col-md-4">
								<label>Tempat Lahir</label>
								<input type="text" placeholder="Tempat Lahir"  name="tmp" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>Tanggal Lahir</label>
								<input type="date" class="form-control pull-right" id="datepicker" name="tgl">
							</div>
						</div>
						<label>Alamat</label>
						<div class="form-group row">
							<div class=" col-md-4">
								<label>Alamat</label>
								<input type="text"  name="alamat" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>RT</label>
								<input type="text"  name="rt" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>RW</label>
								<input type="text"  name="rw" class="form-control" required="">
							</div>	
							<div class=" col-md-4">
								<label>Kabupaten</label>
								<input type="text"  name="kab" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>Kecamatan</label>
								<input type="text"  name="kec" class="form-control" required="">
							</div>
							<div class=" col-md-4">
								<label>Desa / Kelurahan</label>
								<input type="text"  name="des" class="form-control" required="">
							</div>
							
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Biodata Saksi 1</label>
							<textarea class="form-control" rows="3" placeholder="Isi biodata saksi satu" name="bio_saksi1"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Biodata Saksi 2</label>
							<textarea class="form-control" rows="3" placeholder="Isi biodata saksi dua" name="bio_saksi2"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Isi Kompirasi</label>
							<textarea class="form-control" rows="3" placeholder="Isi" name="isi"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Komparisi Pusat</label>
							<textarea class="form-control" rows="3" placeholder="Isi" name="pusat"></textarea>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="status">
								<option value="AKTIF">Aktif</option>
								<option value="TIDAK AKTIF">Tidak Aktif</option>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<button style="float: right;" type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>