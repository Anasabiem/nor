<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
			Customer
			<small>Data Order
				</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Order</li>
		</ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="box-title">
            <?php foreach ($customer->result() as $c): ?>
            <h3>Data Order <?php echo $c->NAMA_CUSTOMER; ?></h3>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="box-body">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalinsert"> <i class="fa fa-plus">Tambah</i></button>
         <!-- Modal Insert-->
            <div class="modal fade" id="exampleModalinsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Order Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     <form autocomplete="off" method="post" action="<?php echo base_url("Customer/prosesTambahDataOrderCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
                      <input type="text" name="id" hidden="" value="">
                      <div class="modal-body">
                        <?php foreach ($rincian1->result() as $view ) { ?>
                        <div class="form-group">
                          <label >No Surat Order : <?php echo $view->NOMOR_SURAT_ORDER_CUSTOMER; ?> || Tanggal : <?php echo date("d F Y", strtotime($view->TANGGAL_SURAT_ORDER_CUSTOMER)); ?> </label>
                        </div>
                        <?php } ?>
                         <?php foreach ($rincian->result() as $view) { ?>
                        <input type="" name="id_Cus" value="<?php echo $view->NO_ID_CUSTOMER ?>" hidden>
                        <?php } ?>
                        <!-- <?php foreach ($rincian2->result() as $view) { ?>
                        <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER ?>" hidden>
                        <?php } ?> -->
                        <?php foreach ($rincian1->result() as $view) { ?>
                        <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER ?>" hidden>
                        <?php } ?>
                        <div class="form-group">
                          <label >Jenis Order</label>
                          <select class="form-control" name="jenis_ord">
                            <?php foreach ($jenisOrder->result() as $view) { ?>
                            <option value="<?php echo $view->NO_ID_JENISORDER ?>"><?php echo $view->NAMA_JENIS_ORDER; ?>-<?php echo $view->OBYEK_JENIS_ORDER; ?>-<?php echo $view->ASAL_OBYEK_JENIS_ORDER; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Order Customer Atas Nama</label>
                          <input type="text" class="form-control" name="order_cus_atas_nama">
                        </div>
                        <div class="form-group">
                          <label >Pasangan Order Customer Atas Nama</label>
                          <input type="text" class="form-control" name="pas_order_cus_atas_nama">
                        </div>
                        <div class="form-group">
                          <label >Biodata Pasangan Order Customer Atas Nama</label>
                          <textarea class="form-control" name="bio_pas_order_cus_atas_nama"></textarea>
                        </div>
                        <div class="form-group">
                          <label >Saksi 1</label>
                          <select class="form-control" name="saksi1">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->NAMA_SAKSI ?>"><?php echo $view->NAMA_SAKSI ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Biodata Saksi 1</label>
                          <select class="form-control" name="bio_saksi1">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->ISI_BIODATA ?>"><?php echo $view->ISI_BIODATA ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Saksi 2</label>
                          <select class="form-control" name="saksi2">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->NAMA_SAKSI ?>"><?php echo $view->NAMA_SAKSI ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Biodata Saksi 2</label>
                          <select class="form-control" name="bio_saksi2">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->ISI_BIODATA ?>"><?php echo $view->ISI_BIODATA ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Nominal Pengikatan</label>
                          <input type="text" class="form-control" name="nominal_pengikatan">
                        </div>
                        <div class="form-group">
                          <label >Keterangan Order Pengikatan</label>
                          <input type="text" class="form-control" name="ket">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        <hr>
        <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Order Customer Atas Nama</th>
              <th>Pasangan Order Customer Atas Nama</th>
              <th>Saksi satu</th>
              <th>Saksi dua</th>
							<th>Nominal Pengikatan</th>
							<th>Keterangan Order Pengikatan</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
            <?php $no=1; foreach ($rincian2->result() as $r): ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?php echo $r->DETAIL_ORDER_ATAS_NAMA; ?></td>
                <td><?php echo $r->PASANGAN_DETAIL_ORDER; ?></td>
                <td><?php echo $r->SAKSI_SATU.'<br>'.substr($r->BIODATA_SAKSI_SATU, 0,150); ?></td>
                <td><?php echo $r->SAKSI_DUA.'<br>'.substr($r->BIODATA_SAKSI_DUA, 0,150); ?></td>
                <td>Rp. <?php echo number_format($r->NOMINAL_DETAIL_ORDER); ?>,00</td>
                <td><?php echo $r->KETERANGAN_DETAIL_ORDER; ?></td>
                <td>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo$r->NO_ID_DETAIL_ORDER ?>">
                    Edit
                  </button>
                  <a class="btn btn-danger" href="<?php echo base_url('Customer/deleteDetailOrderCustomer/').$r->NO_ID_DETAIL_ORDER; ?>">Hapus</a>
                  <?php foreach ($rincian1->result() as $r1): ?>
                    <a class="btn btn-success" href="<?php echo base_url('Customer/detailObyekOrderCus/').$r->NO_ID_DETAIL_ORDER.'/'.$r->NO_ID_ORDER_CUSTOMER.'/'.$r1->NO_ID_CUSTOMER; ?>">Detail Obyek</a>
                  <?php endforeach; ?>
                  <a class="btn btn-warning" href="<?php echo base_url('Customer/dokumen/').$r->NO_ID_DETAIL_ORDER; ?>">Dokumen</a>
                </td>
              </tr>
              <!-- Modal Edit-->
            <div class="modal fade" id="exampleModal<?php echo$r->NO_ID_DETAIL_ORDER ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Order Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     <form autocomplete="off" method="post" action="<?php echo base_url("Customer/updateDatOrderCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
                      <input type="text" name="id" hidden="" value="">
                      <div class="modal-body">
                        <input type="text" name="id_DetOrd" value="<?php echo $r->NO_ID_DETAIL_ORDER ?>" hidden>
                        <input type="text" name="id_OrdCus" value="<?php echo $r->NO_ID_ORDER_CUSTOMER ?>" hidden>
                        <div class="form-group">
                          <label >No ID Order Customer</label>
                          <input type="text" class="form-control" name="no_id_ordCus" value="<?php echo $r->NO_ID_ORDER_CUSTOMER ?>" readonly>
                        </div>
                        <div class="form-group">
                          <label >No ID Jenis Order</label>
                          <select class="form-control" name="jenis_ord">
                            <?php foreach ($jenisOrder->result() as $view) { ?>
                            <option value="<?php echo $view->NO_ID_JENISORDER ?>"><?php echo $view->NAMA_JENIS_ORDER; ?>-<?php echo $view->OBYEK_JENIS_ORDER; ?>-<?php echo $view->ASAL_OBYEK_JENIS_ORDER; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Order Customer Atas Nama</label>
                          <input type="text" class="form-control" name="order_cus_atas_nama" value="<?php echo $r->DETAIL_ORDER_ATAS_NAMA ?>">
                        </div>
                        <div class="form-group">
                          <label >Pasangan Order Customer Atas Nama</label>
                          <input type="text" class="form-control" name="pas_order_cus_atas_nama" value="<?php echo $r->PASANGAN_DETAIL_ORDER ?>">
                        </div>
                        <div class="form-group">
                          <label >Biodata Pasangan Order Customer Atas Nama</label>
                          <textarea class="form-control" name="bio_pas_order_cus_atas_nama"><?php echo $r->BIODATA_PASANGAN_DETAIL_ORDER; ?></textarea>
                        </div>
                        <div class="form-group">
                          <label >Saksi 1</label>
                          <select class="form-control" name="saksi1">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->NAMA_SAKSI ?>"><?php echo $view->NAMA_SAKSI ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Biodata Saksi 1</label>
                          <select class="form-control" name="bio_saksi1">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->ISI_BIODATA ?>"><?php echo $view->ISI_BIODATA ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Saksi 2</label>
                          <select class="form-control" name="saksi2">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->NAMA_SAKSI ?>"><?php echo $view->NAMA_SAKSI ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Biodata Saksi 2</label>
                          <select class="form-control" name="bio_saksi2">
                            <?php foreach ($saksi->result() as $view ) { ?>
                            <option value="<?php echo $view->ISI_BIODATA ?>"><?php echo $view->ISI_BIODATA ?></option>
                           <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Nominal Pengikatan</label>
                          <input type="text" class="form-control" name="nominal_pengikatan" value="<?php echo $r->NOMINAL_DETAIL_ORDER ?>">
                        </div>
                        <div class="form-group">
                          <label >Keterangan Order Pengikatan</label>
                          <input type="text" class="form-control" name="ket" value="<?php echo $r->KETERANGAN_DETAIL_ORDER ?>">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php endforeach; ?>
		      </tbody>
			</table>
      </div>
    </div>
  </section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
