<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
			Customer
			<small>List Detail Obyek Order Customer
				</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">List Detail Obyek Order Customer </li>
		</ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="box-title">
          <?php foreach ($customer->result() as $c): ?>
            <h3>Data Order <?php echo $c->NAMA_CUSTOMER; ?></h3>
            <?php $id_customer = $c->NO_ID_CUSTOMER; ?>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="box-body">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalinsert"> <i class="fa fa-plus">Tambah</i></button>
         <!-- Modal Insert-->
            <div class="modal fade" id="exampleModalinsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Order Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     <form autocomplete="off" method="post" action="<?php echo base_url("Customer/prosesTambahListDetOrdCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
                      <input type="text" name="id" hidden="" value="">
                      <div class="modal-body">
                        <?php foreach ($obyek1->result() as $view ) { ?>
                        <div class="form-group">
                          <label >Obyek Order dan Pengikatan : <?php echo $view->NOMOR_SURAT_ORDER_CUSTOMER; ?> || Tanggal : <?php echo date("d F Y", strtotime($view->TANGGAL_SURAT_ORDER_CUSTOMER)); ?> || Untuk <?php echo $view->KETERANGAN_ORDER_CUSTOMER; ?></label>
                        </div>
                        <?php } ?>

                        <?php foreach ($rincian2->result() as $view) { ?>
                          <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER ?>" hidden>
                        <?php } ?>

                         <?php foreach ($customer->result() as $view ) { ?>
                        <input type="" name="id_Cus" value="<?php echo $view->NO_ID_CUSTOMER?>" hidden >
                         <?php } ?>

                        <?php foreach ($obyek->result() as $view ) { ?>
                        <input type="" name="id_detOrd" value="<?php echo $view->NO_ID_DETAIL_ORDER?>" hidden >
                        <input type="" name="id_jnsOrd" value="<?php echo $view->NO_ID_JENISORDER ?>" hidden >
                         <?php } ?>
                        <div class="form-group">
                          <label >Jenis Obyek Untuk Pengikatan</label>
                          <select class="form-control" name="jenis_oby">
                            <option value="SHM">SHM</option>
                            <option value="SHGB">SHGB</option>
                            <option value="SHGU">SHGU</option>
                            <option value="AJB/LETTER C/PETHOK">AJB/LETTER C/PETHOK</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Nomor Obyek</label>
                          <input type="text" class="form-control" name="nomor_oby">
                        </div>
                        <div class="form-group">
                          <label >Tanggal Obyek</label>
                          <input type="date" class="form-control" name="tgl_oby">
                        </div>
                        <div class="form-group">
                          <label >Jenis Identifikasi Obyek</label>
                          <input type="text" class="form-control" name="jns_iden_oby">
                        </div>
                        <div class="form-group">
                          <label >Nomor Identifikasi Obyek</label>
                          <input type="text" class="form-control" name="nmr_iden_oby">
                        </div>
                        <div class="form-group">
                          <label >Tanggal Identifikasi Obyek</label>
                          <input type="date" class="form-control" name="tgl_iden_oby">
                        </div>
                        <div class="form-group">
                          <label >Obyek Yang Akan Diikat Atas Nama</label>
                          <input type="text" class="form-control" name="ord_atas_nama">
                        </div>
                        <div class="form-group">
                          <label >Luas Obyek Yang Akan Diikat</label>
                          <input type="text" class="form-control" name="luas_oby_diikat">
                        </div>
                        <div class="form-group">
                          <label >Nomor Obyek Pajak(NOP)</label>
                          <input type="text" class="form-control" name="nop">
                        </div>
                        <div class="form-group">
                          <label >Pengikatan Parsial</label>
                          <input type="text" class="form-control" name="parsial">
                        </div>
                        <div class="form-group">
                          <label >Keterangan Lain</label>
                          <input type="text" class="form-control" name="ket">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        <hr>
        <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Typord</th>
							<th>Jenis Obyek</th>
							<th>Luas</th>
							<th>NOP</th>
							<th>Keterangan</th>
              <th>Pengikatan Parsial</th>
              <th>Action</th>
              <!-- <th>Cetak</th> -->
						</tr>
					</thead>
					<tbody>
            <?php $no=1; foreach ($obyek2->result() as $r): ?>
              <tr>
              <?php $id_obyek_order_customer = $r->NO_ID_OBYEK_ORDER_CUSTOMER ?>
                <td><?= $no++ ?></td>
                <?php $typord= $this->db->get_where('0_0_jenis_order',array('NO_ID_JENISORDER'=>$r->NO_ID_JENISORDER))->row_array(); ?>
                <td><?= $typord['NAMA_JENIS_ORDER'].' ('.$typord['OBYEK_JENIS_ORDER'].'-'.$typord['ASAL_OBYEK_JENIS_ORDER'].')' ?></td>
                <td><?php echo
                          '<strong>Jenis Obyek:</strong> <br>'.$r->JENIS_OBYEK.
                          '<br><strong>No:</strong> <br>'.$r->NOMOR_OBYEK.
                          '<br><strong>Tgl:</strong> <br>'.date("d F Y",strtotime($r->TANGGAL_OBYEK)).
                          '<br><strong>Jenis Identifikasi Obyek:</strong> <br>'.$r->JENIS_IDENTIFIKASI_OBYEK.
                          '<br><strong>No Identifikasi Obyek:</strong> <br>'.$r->NOMOR_IDENTIFIKASI_OBYEK.
                          '<br><strong>Tgl Identifikasi:</strong> <br>'.date("d F Y",strtotime($r->TANGGAL_IDENTIFIKASI_OBYEK)).
                          '<br><strong>Atas Nama:</strong> <br>'.$r->ATAS_NAMA_OBYEK; ?></td>
                <td><?php echo $r->LUAS_OBYEK; ?></td>
                <td><?php echo $r->NOMOR_OBYEK_PAJAK; ?></td>
                <td><?php echo $r->KETERANGAN_LAIN_OBYEK; ?></td>
                <td><?php echo number_format($r->NOMINAL_PARTIAL_OBYEK); ?></td>
                <td>
                 <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModaledit<?php echo $r->NO_ID_OBYEK_ORDER_CUSTOMER ?>">Edit</button>
                  <?php foreach ($rincian2->result() as $view) { ?>
                    <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER ?>" hidden>
                  <?php } ?>

                   <?php foreach ($customer->result() as $view1 ) { ?>
                  <input type="" name="id_Cus" value="<?php echo $view1->NO_ID_CUSTOMER?>" hidden >
                   <?php } ?>

                  <?php foreach ($obyek->result() as $view ) { ?>
                  <input type="" name="id_detOrd" value="<?php echo $view->NO_ID_DETAIL_ORDER?>" hidden >
                  <input type="" name="id_jnsOrd" value="<?php echo $view->NO_ID_JENISORDER ?>" hidden >
                   <?php } ?>
                  <a href="<?php echo base_url('Customer/deletelistdetail/').$r->NO_ID_OBYEK_ORDER_CUSTOMER; ?>" class="btn btn-danger">Hapus</a>

                  <?php // foreach ($proses_order->result() as $por): ?>
                    <?php $id_obyek_match = $this->M_model->selectwhere('2_0_proses_order_customer', array('NO_ID_OBYEK_ORDER_CUSTOMER'=>$id_obyek_order_customer)); ?>
                  <?php // endforeach; ?>

                  <?php foreach ($obyek->result() as $o): ?>
                    <?php if ($id_obyek_match->num_rows()==0): ?>
                      <a href="<?php echo base_url('Customer/inputProsesPengikatan/').$r->NO_ID_OBYEK_ORDER_CUSTOMER.'/'.$o->NO_ID_JENISORDER.'/'.$o->NO_ID_ORDER_CUSTOMER.'/'.$r->NO_ID_DETAIL_ORDER; ?>" class="btn btn-warning">Proses</a>
                    <?php else: ?>
                      <?php if ($this->session->userdata('tipene')== 1 || $this->session->userdata('tipene')== 4 ): ?>
                        <a href="<?php echo base_url('ProsesPengikatan/listProsesPengikatanObyek/').$r->NO_ID_OBYEK_ORDER_CUSTOMER.'/'.$o->NO_ID_JENISORDER.'/'.$id_customer.'/'.$o->NO_ID_ORDER_CUSTOMER.'/'.$r->NO_ID_DETAIL_ORDER; ?>" class="btn btn-success">List View</a>
                      <?php endif; ?>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </td>
                <!-- <td>
                  <a class="btn btn-primary" href="<?php echo base_url('Customer/cetakAPHT/').$view->NO_ID_DETAIL_ORDER.'/'.$o->NO_ID_ORDER_CUSTOMER.'/'.$view1->NO_ID_CUSTOMER.'/'.$r->NO_ID_OBYEK_ORDER_CUSTOMER ?>" name="" >APHT</a>
                  <a class="btn btn-primary" href="<?php echo base_url('Customer/cetakSKMHT/').$view->NO_ID_DETAIL_ORDER.'/'.$o->NO_ID_ORDER_CUSTOMER.'/'.$view1->NO_ID_CUSTOMER.'/'.$r->NO_ID_OBYEK_ORDER_CUSTOMER ?>" name="" >SKHMT</a>
                  <a class="btn btn-primary" href="<?php echo base_url('Customer/cetakHIBAH/').$view->NO_ID_DETAIL_ORDER.'/'.$o->NO_ID_ORDER_CUSTOMER.'/'.$view1->NO_ID_CUSTOMER.'/'.$r->NO_ID_OBYEK_ORDER_CUSTOMER ?>" name="" >HIBAH</a>
                </td> -->
              </tr>
               <!-- Modal Edit-->
            <div class="modal fade" id="exampleModaledit<?php echo $r->NO_ID_OBYEK_ORDER_CUSTOMER ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Order Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     <form autocomplete="off" method="post" action="<?php echo base_url("Customer/UpdateListDetOrdCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
                      <input type="text" name="id" hidden="" value="">
                      <div class="modal-body">
                        <?php foreach ($obyek1->result() as $view ) { ?>
                        <div class="form-group">
                          <label >Obyek Order dan Pengikatan : <?php echo $view->NOMOR_SURAT_ORDER_CUSTOMER; ?> || Tanggal : <?php echo date("d F Y", strtotime($view->TANGGAL_SURAT_ORDER_CUSTOMER)); ?> || Untuk <?php echo $view->KETERANGAN_ORDER_CUSTOMER; ?></label>
                        </div>
                        <?php } ?>
                         <?php foreach ($rincian2->result() as $view ) { ?>
                        <input type="" name="id_OrdCus" value="<?php echo $view->NO_ID_ORDER_CUSTOMER?>" hidden>
                         <?php } ?>

                         <?php foreach ($customer->result() as $view ) { ?>
                        <input type="" name="id_Cus" value="<?php echo $view->NO_ID_CUSTOMER?>" hidden>
                         <?php } ?>

                        <input type="" name="id_ObyOrdCus" value="<?php echo $r->NO_ID_OBYEK_ORDER_CUSTOMER ?>" hidden>
                        <?php foreach ($obyek->result() as $view ) { ?>
                        <input type="" name="id_detOrd" value="<?php echo $view->NO_ID_DETAIL_ORDER?>" hidden>
                        <input type="" name="id_jnsOrd" value="<?php echo $view->NO_ID_JENISORDER ?>" hidden>
                         <?php } ?>
                        <div class="form-group">
                          <label >Jenis Obyek Untuk Pengikatan</label>
                          <select class="form-control" name="jenis_oby">
                            <option value="SHM">SHM</option>
                            <option value="SHGB">SHGB</option>
                            <option value="SHGU">SHGU</option>
                            <option value="AJB/LETTER C/PETHOK">AJB/LETTER C/PETHOK</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Nomor Obyek</label>
                          <input type="text" class="form-control" value="<?php echo $r->NOMOR_OBYEK ?>" name="nomor_oby">
                        </div>
                        <div class="form-group">
                          <label >Tanggal Obyek</label>
                          <input type="date" class="form-control" value="<?php echo $r->TANGGAL_OBYEK ?>" name="tgl_oby">
                        </div>
                        <div class="form-group">
                          <label >Jenis Identifikasi Obyek</label>
                          <input type="text" class="form-control" value="<?php echo $r->JENIS_IDENTIFIKASI_OBYEK ?>" name="jns_iden_oby">
                        </div>
                        <div class="form-group">
                          <label >Nomor Identifikasi Obyek</label>
                          <input type="text" class="form-control" value="<?php echo $r->NOMOR_IDENTIFIKASI_OBYEK ?>" name="nmr_iden_oby">
                        </div>
                        <div class="form-group">
                          <label >Tanggal Identifikasi Obyek</label>
                          <input type="date" class="form-control" value="<?php echo $r->TANGGAL_IDENTIFIKASI_OBYEK ?>" name="tgl_iden_oby">
                        </div>
                        <div class="form-group">
                          <label >Obyek Yang Akan Diikat Atas Nama</label>
                          <input type="text" class="form-control" value="<?php echo $r->ATAS_NAMA_OBYEK ?>" name="ord_atas_nama">
                        </div>
                        <div class="form-group">
                          <label >Luas Obyek Yang Akan Diikat</label>
                          <input type="text" class="form-control" value="<?php echo $r->LUAS_OBYEK ?>" name="luas_oby_diikat">
                        </div>
                        <div class="form-group">
                          <label >Nomor Obyek Pajak(NOP)</label>
                          <input type="text" class="form-control" value="<?php echo $r->NOMOR_OBYEK_PAJAK ?>" name="nop">
                        </div>
                        <div class="form-group">
                          <label >Pengikatan Parsial</label>
                          <input type="text" class="form-control" value="<?php echo $r->NOMINAL_PARTIAL_OBYEK ?>" name="parsial">
                        </div>
                        <div class="form-group">
                          <label >Keterangan Lain</label>
                          <input type="text" class="form-control" value="<?php echo $r->KETERANGAN_LAIN_OBYEK ?>" name="ket">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <?php endforeach; ?>
		      </tbody>  
			</table>
      </div>
    </div>
  </section>
</div>

<?php $this->load->view('side/footer') ?>
<?php $this->load->view('side/js') ?>
