<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Customer
			<small>Data Customer 
			</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Customer <?php foreach ($pemilik->result() as $key): ?>
				<?php echo $key->NAMA_CUSTOMER ?>
				<?php endforeach ?></h3>
			</div>
			<div class="box-body">
				<a class="btn btn-primary" href="<?php echo base_url('Customer/ieCus/'.$key->NO_ID_CUSTOMER) ?>"><i class="fa fa-plus"> Tambah </i></a>
				<hr>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Pemimpin</th>
							<th>No KTP</th>
							<th>Alamat</th>
							<th>Isi</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($detail->result() as $menu): ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><a href="" data-toggle="modal" data-target="#modal-default<?php echo $menu->NO_ID_DETAIL_CUSTOMER ?>"><?php echo $menu->NAMA_COMPARATOR_CUSTOMER ?></a></td>
							<td><?php echo $menu->NO_KTP_COMPARATOR_CUSTOMER ?>, <?php echo $menu->TEMPAT_LAHIR ?>, <?php echo $menu->TANGGAL_LAHIR ?></td>
							<td><?php echo $menu->ALAMAT_COMPARATOR_CUSTOMER ?>, RT <?php echo $menu->RT_COMPARATOR_CUSTOMER ?>, RW <?php echo $menu->RW_COMPARATOR_CUSTOMER ?> Desa <?php echo $menu->DESA_KELURAHAN_COMPARATOR_CUSTOMER ?> Kecamatan <?php echo $menu->KECAMATAN_COMPARATOR_CUSTOMER ?>,<?php echo $menu->KABUPATEN_COMPARATOR_CUSTOMER ?></td>
							<td><?php echo $menu->ISI_COMPARATOR_CUSTOMER ?></td>
							<td><?php echo $menu->STATUS_COMPARATOR ?></td>
							<td>
								<a href="<?php echo base_url('Customer/editDetailCus/'.$menu->NO_ID_DETAIL_CUSTOMER) ?>" class="btn btn-social-icon "><i class="fa fa-pencil" title="Edit"></i></a>
								<a href="<?php echo base_url('Customer/deleteDetCus/'.$menu-> NO_ID_DETAIL_CUSTOMER) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-social-icon"><i class="fa fa-trash" title="Hapus" style="color: red"></i></a>
							</td>
						</tr>

						<div class="modal fade" id="modal-default<?php echo $menu->NO_ID_DETAIL_CUSTOMER ?>">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Tambah Menu Baru</h4>
										</div>
										<div class="modal-body">
											<form class="form-horizontal" method="POST" action="<?php echo base_url('Customer/updateComPusat') ?>">
												<div class="box-body">
													<div class="form-group">
														<label >Id Pemimpin</label>
														<input type="text" readonly="" class="form-control" value="<?php echo $menu->NO_ID_DETAIL_CUSTOMER ?>" name="idDet">
													</div>
													<div class="form-group">
														<label for="exampleInputPassword1">Nama Pemimpin</label>
														<input type="text" class="form-control" name="nama" required="" value="<?php echo $menu->NAMA_COMPARATOR_CUSTOMER ?>">
													</div>
													<div class="form-group">
														<label for="inputPassword3" >Komparisi Pusat</label>
														<textarea class="form-control" name="kompa"><?php echo $menu->KOMPARISI_PUSAT ?></textarea>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-primary">Simpan </button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('side/footer') ?>

<?php $this->load->view('side/js') ?>