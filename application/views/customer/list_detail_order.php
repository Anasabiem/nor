<?php $this->load->view('side/head') ?>
<?php $this->load->view('side/navbar') ?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Customer
			<small>Data Customer
				</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Customer</a></li>
			<li class="active">Data Customer</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Data Customer <?php foreach ($pemilik->result() as $key): ?>
					<?php echo $key->NAMA_CUSTOMER ?>
				<?php endforeach ?></h3>
			</div>
			<div class="box-body">
				<button class="btn btn-primary" data-toggle="modal" data-target="#modal-default"> <i class="fa fa-plus">Tambah</i></button>
				<div class="modal fade" id="modal-default">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Tambah Proses Order</h4>
								</div>
								<div class="modal-body">
									<form class="form-horizontal" method="POST" action="<?php echo base_url('Customer/prosesTambahProsesOrder') ?>">
										<div class="box-body">
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">Nama Customer</label>
												<div class="col-sm-10">
													<input type="text" name="parent" value="0" hidden>
													<?php foreach ($pemilik->result() as $p): ?>
														<input type="text" class="form-control" placeholder="Nama Menu" value="<?php echo $p->NAMA_CUSTOMER; ?>" readonly>
														<input type="text" name="no_id_customer" value="<?php echo $p->NO_ID_CUSTOMER; ?>" hidden>
													<?php endforeach; ?>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Nomor Surat Order Customer</label>
												<div class="col-sm-10">
													<input type="text" class="form-control"  name="no_surat_order_customer">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Tanggal Surat</label>
												<div class="col-sm-10">
													<input type="date" class="form-control" name="tgl_surat">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Nomor Cover Note Notaris</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="no_cover_note_notaris">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Tanggal Cover Note Notaris</label>
												<div class="col-sm-10">
													<input type="date" class="form-control" name="tgl_cover_note_notaris">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Keterangan Order</label>
												<div class="col-sm-10">
													<textarea class="form-control" name="ket_order" rows="8" cols="80"></textarea>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Simpan </button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				<hr>
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor Surat Order</th>
							<th>Tanggal Surat</th>
							<th>Nomor Cover Order</th>
							<th>Tanggal Cover Note</th>
							<th>Keterangan</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($orderDetail->result() as $menu): ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><a href=""><?php echo $menu->NOMOR_SURAT_ORDER_CUSTOMER ?></a></td>
							<td><?php echo date("d F Y", strtotime($menu->TANGGAL_SURAT_ORDER_CUSTOMER)) ?></td>
							<td><?php echo $menu->NOMOR_LAIN_ORDER_CUSTOMER ?></td>

							<td><?php echo date("d F Y", strtotime($menu->TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER)) ?></td>
							<td><?php echo $menu->KETERANGAN_ORDER_CUSTOMER ?></td>
							<td>
								<!-- <form action="<?php // echo base_url('Customer/detailRincianOrderCus'); ?>" method="post"> -->
									<!-- <input type="text" name="no_id_order_customer" value="<?php // echo $menu->NO_ID_ORDER_CUSTOMER; ?>"> -->
									<!-- <input type="text" name="no_id_customer" value="<?php // echo $menu->NO_ID_CUSTOMER; ?>"> -->
									<!-- <a href="<?php echo base_url('Customer/detailRincianOrderCus/').$menu->NO_ID_ORDER_CUSTOMER; ?>" class="btn btn-primary">Edit</a> -->
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo$menu->NO_ID_ORDER_CUSTOMER ?>">
									  Edit
									</button>
									<?php foreach ($pemilik->result() as $view) { ?>
									<input type="" name="id_ordCus" value="<?php echo $view->NO_ID_CUSTOMER ?>" hidden>
									<?php } ?>
									<a href="<?php echo base_url('Customer/deleteDataCustomer/'.$menu->NO_ID_ORDER_CUSTOMER) ?>" title="hapus" onclick="javascript: return confirm('Anda Yakin Akan Menghapus ?')" class="btn btn-danger">Hapus</a>
									<a href="<?php echo base_url('Customer/detailRincianOrderCus/').$menu->NO_ID_ORDER_CUSTOMER.'/'.$menu->NO_ID_CUSTOMER; ?>" class="btn btn-warning">Rincian</a>
								<!-- </form> -->
						</td>

						</tr>
						<!-- Modal -->
						<div class="modal fade" id="exampleModal<?php echo$menu->NO_ID_ORDER_CUSTOMER ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Edit Data Customer</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						      	 <form autocomplete="off" method="post" action="<?php echo base_url("Customer/updateDataCus")?>" enctype="multipart/form-data" class="form-horizontal form-bordered">
						          <input type="text" name="id" hidden="" value="">
						          <div class="modal-body">
						          	<input type="text" name="id_ordCus" value="<?php echo $menu->NO_ID_ORDER_CUSTOMER ?>" >
						            <input type="text" name="idCus" value="<?php echo $menu->NO_ID_CUSTOMER ?>" >
						            <div class="form-group">
						              <label >Nomor Surat Order</label>
						              <input type="text" class="form-control" name="no_surat" value="<?php echo $menu->NOMOR_SURAT_ORDER_CUSTOMER ?>">
						            </div>
						            <div class="form-group">
						              <label >Tanggal Surat</label>
						              <input type="date" class="form-control" name="tgl_surat" value="<?php echo $menu->TANGGAL_SURAT_ORDER_CUSTOMER ?>">
						            </div>
						            <div class="form-group">
						              <label >Nomor Cover Order</label>
						              <input type="text" class="form-control" name="no_cover_order" value="<?php echo $menu->NOMOR_LAIN_ORDER_CUSTOMER ?>">
						            </div>
						            <div class="form-group">
						              <label >Tanggal Cover Note</label>
						              <input type="date" class="form-control" name="tgl_cover_note" value="<?php echo $menu->TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER ?>">
						            </div>
						            <div class="form-group">
						              <label >Keterangan</label>
						              <input type="text" class="form-control" name="ket" value="<?php echo $menu->KETERANGAN_ORDER_CUSTOMER ?>">
						            </div>
						          </div>
						          <div class="modal-footer">
						            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
						            <button type="submit" value="upload" class="btn btn-primary">Simpan</button>
						          </div>
						        </form>
						      </div>
						  </div>
						</div>

					<?php endforeach ?>
				</tbody>
				<tfoot>
					<tr>
							<th>No</th>
							<th>Nomor Surat</th>
							<th>Tanggal Surat</th>
							<th>Nomor Lain Order</th>
							<th>Tanggal Nomor Lain Order</th>
							<!-- <th>Komparasi Pusat</th> -->
							<th>Keterangan</th>
							<th>Action</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
</div>

<<<<<<< HEAD
=======
<?php $this->load->view('side/footer') ?>
>>>>>>> 1d895f0dae748b6cf7782752cf77946070f5ea8f
<?php $this->load->view('side/js') ?>
