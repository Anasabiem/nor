<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_model extends CI_Model {

  public function select($table){
    return $this->db->get($table);
  }

  public function selectwhereorder($table,$data,$isi,$dsc){
    $this->db->order_by($isi,$dsc);
    return $this->db->get_where($table, $data);
  }

  public function selectwhere($table,$data){
    return $this->db->get_where($table, $data);
  }

  public function delete($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  }

  public function update($table,$data,$where){
    $this->db->update($table,$data,$where);
  }

  public function insert($table,$data){
    $this->db->insert($table,$data);
  }

  public function cek_login($table,$where){
    return $this->db->get_where($table,$where);
  }

  public function ambyar(){
    $this->db->select('menu.*,0_user.*,member_menu.*');
    $this->db->join('member_menu', 'member_menu.MENU_ID = menu.id');
    $this->db->join('0_user', '0_user.ID_USER = member_menu.MEMBER_ID');
    $this->db->where('menu.parent_id',0);
    $this->db->where('0_user.ID_USER',$this->session->userdata('id'));
    $this->db->from('menu');
    $this->db->order_by('menu_order','ASC');
    return $this->db->get();
  }

  public function anakan($id){
    $this->db->select('menu.*,0_user.*,member_menu.*');
    $this->db->join('member_menu', 'member_menu.MENU_ID = menu.id');
    $this->db->join('0_user', '0_user.ID_USER = member_menu.MEMBER_ID');
    $this->db->where('menu.parent_id',$id);
    $this->db->where('0_user.ID_USER',$this->session->userdata('id'));
    $this->db->from('menu');
    $this->db->order_by('menu_order','ASC');
    return $this->db->get();
  }

  public function prosesPengikatan($id){
    $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*, 0_0_jenis_order.*, 1_2_detail_obyek_order_customer.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('0_0_proses_order', '2_0_proses_order_customer.NO_ID_PROSES_ORDER = 0_0_proses_order.NO_ID_PROSES_ORDER');
    $this->db->join('0_0_jenis_order', '2_0_proses_order_customer.NO_ID_JENISORDER = 0_0_jenis_order.NO_ID_JENISORDER');
    $this->db->join('1_2_detail_obyek_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->where('2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER', $id);
    $data=$this->db->get();
    return $data;
  }

  public function laporan($date_from, $date_to){
    $this->db->select('2_0_proses_order_customer.*, 1_2_detail_obyek_order_customer.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('1_2_detail_obyek_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->where('2_0_proses_order_customer.TGL_AKTA >=', $date_from);
    $this->db->where('2_0_proses_order_customer.TGL_AKTA <=', $date_to);
    $data=$this->db->get();
    return $data;
  }

  public function jumlah_data(){
    return $this->db->get('0_user')->num_rows();
  }

  public function data($number,$offset){
    $this->db->order_by('STATUS_LOGIN', 'DESC');
		return $query = $this->db->get('0_user',$number,$offset)->result();
	}
  public function get_pembagian(){
    $this->db->select('0_user.*, 0_3_user_cust.*');
    $this->db->from('0_user');
    $this->db->join('0_3_user_cust', '0_3_user_cust.NO_ID_USR = 0_user.ID_USER');
    $this->db->group_by('0_3_user_cust.NO_ID_USR');
    $data=$this->db->get();
    return $data;
  }
  public function get_pembagian_cust($where){
    $this->db->select('0_1_data_customer.*, 0_3_user_cust.*');
    $this->db->from('0_1_data_customer');
    $this->db->join('0_3_user_cust', '0_3_user_cust.NO_ID_CUST = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->where('0_3_user_cust.NO_ID_USR',$where);
    $data=$this->db->get();
    return $data;
  }
}
