<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proses extends CI_Model {
  public function proses(){
    // $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*, 0_0_jenis_order.*,0_1_data_customer.*,0_1_detail_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*,1_1_detail_order_customer.*');
    $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*, 0_0_jenis_order.*, 1_2_detail_obyek_order_customer.*, 1_1_detail_order_customer.*,1_0_data_order_customer.*,0_1_data_customer.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('0_0_proses_order', '2_0_proses_order_customer.NO_ID_PROSES_ORDER = 0_0_proses_order.NO_ID_PROSES_ORDER');
    $this->db->join('0_0_jenis_order', '2_0_proses_order_customer.NO_ID_JENISORDER = 0_0_jenis_order.NO_ID_JENISORDER');
    //$this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_2_detail_obyek_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_ORDER_CUSTOMER = 1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('0_1_data_customer', '0_1_data_customer.NO_ID_CUSTOMER = 1_0_data_order_customer.NO_ID_CUSTOMER');
    // $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->order_by('2_0_proses_order_customer.NO_ID_PROSES_ORDER_CUSTOMER','DESC');
    // $this->db->where('2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER', $id);
    $data=$this->db->get();
    return $data;
  }
  public function prosesByUser(){
    $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*, 0_0_jenis_order.*,0_1_data_customer.*,0_1_detail_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*,1_1_detail_order_customer.*');
    $this->db->from('2_0_proses_order_customer');

    $this->db->join('0_0_proses_order', '2_0_proses_order_customer.NO_ID_PROSES_ORDER = 0_0_proses_order.NO_ID_PROSES_ORDER');

    $this->db->join('0_0_jenis_order', '2_0_proses_order_customer.NO_ID_JENISORDER = 0_0_jenis_order.NO_ID_JENISORDER');

    $this->db->join('1_2_detail_obyek_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');

    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');

    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_ORDER_CUSTOMER = 1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER');

    $this->db->join('0_1_data_customer', '0_1_data_customer.NO_ID_CUSTOMER = 1_0_data_order_customer.NO_ID_CUSTOMER');

    $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
  // $this->db->order_by('2_0_proses_order_customer.NO_ID_PROSES_ORDER_CUSTOMER','DESC');
    $this->db->where('2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER', $this->uri->segment(3));

    $data=$this->db->get();
    return $data;
  }

  public function get_proses_customer(){
    $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('0_0_proses_order', '0_0_proses_order.NO_ID_PROSES_ORDER = 2_0_proses_order_customer.NO_ID_PROSES_ORDER');
    $this->db->where('2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER',$this->uri->segment(3));
    if ($this->session->userdata('tipene') == 2) {
      $this->db->where('0_0_proses_order.OUTPUT',1);
    } else if($this->session->userdata('tipene') == 5) {
      $this->db->where('0_0_proses_order.OUTPUT',2);
    }
    return $this->db->get();
  }

  public function proses_customer(){
    $get_session_type_user = $this->session->userdata('tipene'); 

    if ($get_session_type_user == 4 || $get_session_type_user== 1 || $get_session_type_user== 5 ) {
      $this->db->select('0_1_data_customer.*, 1_0_data_order_customer.*, 1_1_detail_order_customer.*,1_2_detail_obyek_order_customer.*,2_0_proses_order_customer.*');
      $this->db->from('0_1_data_customer');

      $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');

      $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');

      $this->db->join('1_2_detail_obyek_order_customer','1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');

      $this->db->join('2_0_proses_order_customer','2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');

      $this->db->group_by('1_1_detail_order_customer.NO_ID_DETAIL_ORDER');

      return $this->db->get();

    } else {

      $this->db->select('0_1_data_customer.*, 1_0_data_order_customer.*, 1_1_detail_order_customer.*,0_3_user_cust.*, 1_2_detail_obyek_order_customer.*,2_0_proses_order_customer.*');
      $this->db->from('0_1_data_customer');

      $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');

      $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');

      $this->db->join('0_3_user_cust', '0_3_user_cust.NO_ID_CUST = 0_1_data_customer.NO_ID_CUSTOMER');

      $this->db->join('1_2_detail_obyek_order_customer','1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');

      $this->db->join('2_0_proses_order_customer','2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMERs');

      $this->db->group_by('1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');

      $this->db->where('0_3_user_cust.NO_ID_USR',$this->session->userdata('id'));

      return $this->db->get();
    }

  }

  public function get_list_harga_proses_admin(){
    $this->db->select('0_1_data_customer.*, 1_0_data_order_customer.*, 1_1_detail_order_customer.*');
      $this->db->from('0_1_data_customer');

      $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');

      $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');

      // $this->db->join('1_2_detail_obyek_order_customer','1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');

      // $this->db->join('2_0_proses_order_customer','2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');

      // $this->db->group_by('1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');

      return $this->db->get();
  }

  public function cetakApht(){
    $id_customer = $this->uri->segment(5);
    $id_detail_order = $this->uri->segment(3);
    $id_order_customer = $this->uri->segment(4);
    $id_obyek_order_customer = $this->uri->segment(6);
    $this->db->select('0_1_data_customer.*, 0_1_detail_customer.*,1_1_detail_order_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*');
    $this->db->from('0_1_data_customer');
    $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('1_2_detail_obyek_order_customer', '1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_1_detail_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->where('1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER', $id_obyek_order_customer);
    $data=$this->db->get();
    return $data;
  }
  public function cetakSKMHT(){
    $id_customer = $this->uri->segment(5);
    $id_detail_order = $this->uri->segment(3);
    $id_order_customer = $this->uri->segment(4);
    $id_obyek_order_customer = $this->uri->segment(6);
    $this->db->select('0_1_data_customer.*, 0_1_detail_customer.*,1_1_detail_order_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*');
    $this->db->from('0_1_data_customer');
    $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('1_2_detail_obyek_order_customer', '1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_1_detail_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->where('1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER', $id_obyek_order_customer);
    $data=$this->db->get();
    return $data;
  }
  public function cetakHIBAH(){
    $id_customer = $this->uri->segment(5);
    $id_detail_order = $this->uri->segment(3);
    $id_order_customer = $this->uri->segment(4);
    $id_obyek_order_customer = $this->uri->segment(6);
    $this->db->select('0_1_data_customer.*, 0_1_detail_customer.*,1_1_detail_order_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*');
    $this->db->from('0_1_data_customer');
    $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('1_2_detail_obyek_order_customer', '1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_1_detail_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->where('1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER', $id_obyek_order_customer);
    $data=$this->db->get();
    return $data;
  }

  public function cetakAphtbyUser(){
    $id_proses_order_customer = $this->uri->segment(4);
    $this->db->select('2_0_proses_order_customer.*, 1_2_detail_obyek_order_customer.*, 1_1_detail_order_customer.*, 1_0_data_order_customer.*, 0_0_jenis_order.*, 0_0_proses_order.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('1_2_detail_obyek_order_customer', '1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_ORDER_CUSTOMER = 1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('0_0_jenis_order', '0_0_jenis_order.NO_ID_JENISORDER = 2_0_proses_order_customer.NO_ID_JENISORDER');
    $this->db->join('0_0_proses_order', '0_0_proses_order.NO_ID_PROSES_ORDER = 2_0_proses_order_customer.NO_ID_PROSES_ORDER');
    $this->db->where('2_0_proses_order_customer.NO_ID_PROSES_ORDER_CUSTOMER', $id_proses_order_customer);
    $data=$this->db->get();
    return $data->result();
  }

  public function cetakSKMHTbyUser(){
    $id_customer = $this->uri->segment(5);
    $id_detail_order = $this->uri->segment(3);
    $id_order_customer = $this->uri->segment(4);
    $id_obyek_order_customer = $this->uri->segment(3);
    $id_proses_order_customer = $this->uri->segment(4);
  // $this->db->select('0_1_data_customer.*, 0_1_detail_customer.*,1_1_detail_order_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*,2_0_proses_order_customer.*');
  // $this->db->from('0_1_data_customer');
  // $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
  // $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
  // $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');
  // $this->db->join('1_2_detail_obyek_order_customer', '1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_1_detail_order_customer.NO_ID_DETAIL_ORDER');
  // $this->db->join('2_0_proses_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*, 0_0_jenis_order.*,0_1_data_customer.*,0_1_detail_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*,1_1_detail_order_customer.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('0_0_proses_order', '2_0_proses_order_customer.NO_ID_PROSES_ORDER = 0_0_proses_order.NO_ID_PROSES_ORDER');
    $this->db->join('0_0_jenis_order', '2_0_proses_order_customer.NO_ID_JENISORDER = 0_0_jenis_order.NO_ID_JENISORDER');
  //$this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_2_detail_obyek_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_ORDER_CUSTOMER = 1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('0_1_data_customer', '0_1_data_customer.NO_ID_CUSTOMER = 1_0_data_order_customer.NO_ID_CUSTOMER');
    $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->where('2_0_proses_order_customer.NO_ID_PROSES_ORDER_CUSTOMER', $id_order_customer);
    $data=$this->db->get();
    return $data;
  }
  public function cetakHIBAHbyUser(){
    $id_customer = $this->uri->segment(5);
    $id_detail_order = $this->uri->segment(3);
    $id_order_customer = $this->uri->segment(4);
    $id_obyek_order_customer = $this->uri->segment(3);
  // $this->db->select('0_1_data_customer.*, 0_1_detail_customer.*,1_1_detail_order_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*');
  // $this->db->from('0_1_data_customer');
  // $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
  // $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
  // $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER = 1_0_data_order_customer.NO_ID_ORDER_CUSTOMER');
  // $this->db->join('1_2_detail_obyek_order_customer', '1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER = 1_1_detail_order_customer.NO_ID_DETAIL_ORDER');
  // $this->db->join('2_0_proses_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
  // $this->db->where('2_0_proses_order_customer.NO_ID_PROSES_ORDER_CUSTOMER', $id_order_customer);
    $this->db->select('2_0_proses_order_customer.*, 0_0_proses_order.*, 0_0_jenis_order.*,0_1_data_customer.*,0_1_detail_customer.*, 1_2_detail_obyek_order_customer.*,1_0_data_order_customer.*,1_1_detail_order_customer.*');
    $this->db->from('2_0_proses_order_customer');
    $this->db->join('0_0_proses_order', '2_0_proses_order_customer.NO_ID_PROSES_ORDER = 0_0_proses_order.NO_ID_PROSES_ORDER');
    $this->db->join('0_0_jenis_order', '2_0_proses_order_customer.NO_ID_JENISORDER = 0_0_jenis_order.NO_ID_JENISORDER');
  //$this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->join('1_2_detail_obyek_order_customer', '2_0_proses_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER = 1_2_detail_obyek_order_customer.NO_ID_OBYEK_ORDER_CUSTOMER');
    $this->db->join('1_1_detail_order_customer', '1_1_detail_order_customer.NO_ID_DETAIL_ORDER = 1_2_detail_obyek_order_customer.NO_ID_DETAIL_ORDER');
    $this->db->join('1_0_data_order_customer', '1_0_data_order_customer.NO_ID_ORDER_CUSTOMER = 1_1_detail_order_customer.NO_ID_ORDER_CUSTOMER');
    $this->db->join('0_1_data_customer', '0_1_data_customer.NO_ID_CUSTOMER = 1_0_data_order_customer.NO_ID_CUSTOMER');
    $this->db->join('0_1_detail_customer', '0_1_detail_customer.NO_ID_CUSTOMER = 0_1_data_customer.NO_ID_CUSTOMER');
    $this->db->where('2_0_proses_order_customer.NO_ID_PROSES_ORDER_CUSTOMER', $id_order_customer);
    $data=$this->db->get();
    return $data;
  }
}
