<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	function __construct(){
			parent::__construct();
			$this->load->model('M_proses');
		}

	public function index(){
		$data['cus']=$this->M_model->select('0_1_data_customer')->result();
		$this->load->view('customer/v_customer',$data);
	}

	public function tambah(){
		$this->load->view('customer/i_customer');
	}

	public function tambahCustomer(){
		$data = array('NAMA_CUSTOMER' => $this->input->post('nm_cus'),
						'JENIS_CUSTOMER'=>$this->input->post('jenis'),
						'ALAMAT_CUSTOMER'=> $this->input->post('alamat'),
						'KABUPATEN_CUSTOMER'=>$this->input->post('kab'),
						'KECAMATAN_CUSTOMER'=>$this->input->post('kec'),
						'DESA_KELURAHAN_CUSTOMER'=>$this->input->post('des'),
						'RT_CUSTOMER'=>$this->input->post('rt'),
						'RW_CUSTOMER'=>$this->input->post('rw'),
						'NOMOR_TELEPHONE_CUSTOMER'=>$this->input->post('telp'),
						'PIC_CUSTOMER'=>$this->input->post('pic'));
		$this->M_model->insert('0_1_data_customer',$data);
		redirect(base_url('Customer'));
	}

	public function hapusCustomer($id){
		$where = array('NO_ID_CUSTOMER'=>$id);
		$this -> M_model -> delete($where,'0_1_data_customer');
		header('location:'.base_url('Customer'));
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$data['dataCus']=$this->M_model->selectwhere('0_1_data_customer',array('NO_ID_CUSTOMER'=>$id))->result();
		$this->load->view('customer/e_customer',$data);
	}

	public function updateCustomer(){
		$where = array('NO_ID_CUSTOMER'=>$this->input->post('idCus'));
		$data = array('NAMA_CUSTOMER' => $this->input->post('nm_cus'),
						'JENIS_CUSTOMER'=>$this->input->post('jenis'),
						'ALAMAT_CUSTOMER'=> $this->input->post('alamat'),
						'KABUPATEN_CUSTOMER'=>$this->input->post('kab'),
						'KECAMATAN_CUSTOMER'=>$this->input->post('kec'),
						'DESA_KELURAHAN_CUSTOMER'=>$this->input->post('des'),
						'RT_CUSTOMER'=>$this->input->post('rt'),
						'RW_CUSTOMER'=>$this->input->post('rw'),
						'NOMOR_TELEPHONE_CUSTOMER'=>$this->input->post('telp'),
						'PIC_CUSTOMER'=>$this->input->post('pic'));
		// die(var_dump($data));
		$this->M_model->update('0_1_data_customer',$data,$where);
		redirect(base_url('Customer'));
	}

	public function goDet(){
		$data['detail']=$this->M_model->selectwhere('0_1_detail_customer',array('NO_ID_CUSTOMER'=>$this->uri->segment(3)));
		$data['pemilik']=$this->M_model->selectwhere('0_1_data_customer',array('NO_ID_CUSTOMER'=>$this->uri->segment(3)));
		// die(var_dump(expression));
		$this->load->view('customer/d_customer',$data);
	}

	public function ieCus(){
		$data['pemilik']=$this->M_model->selectwhere('0_1_data_customer',array('NO_ID_CUSTOMER'=>$this->uri->segment(3)));
		$this->load->view('customer/i_e_customer',$data);
	}

	public function tambahDetCus(){
		$data = array('NO_ID_CUSTOMER' =>$this->input->post('idCus'),
					'NAMA_COMPARATOR_CUSTOMER'=>$this->input->post('nm_pem'),
					'PENERIMA'=>$this->input->post('nm_penerima'),
					'PEMBERI'=>$this->input->post('nm_pemberi'),
					'ISTRI_PEMBERI'=>$this->input->post('istri_pemberi'),
					'SAKSI_SATU'=>$this->input->post('saksi1'),
					'SAKSI_DUA'=>$this->input->post('saksi2'),
					'NO_KTP_COMPARATOR_CUSTOMER'=>$this->input->post('ktp'),
					'ALAMAT_COMPARATOR_CUSTOMER'=>$this->input->post('alamat'),
					'TEMPAT_LAHIR'=>$this->input->post('tmp'),
					'TANGGAL_LAHIR'=>$this->input->post('tgl'),
					'KABUPATEN_COMPARATOR_CUSTOMER'=>$this->input->post('kab'),
					'KECAMATAN_COMPARATOR_CUSTOMER'=>$this->input->post('kec'),
					'DESA_KELURAHAN_COMPARATOR_CUSTOMER'=>$this->input->post('des'),
					'RT_COMPARATOR_CUSTOMER'=>$this->input->post('rt'),
					'RW_COMPARATOR_CUSTOMER'=>$this->input->post('rw'),
					'BIODATA_SAKSI_SATU'=>$this->input->post('bio_saksi1'),
					'BIODATA_SAKSI_DUA'=>$this->input->post('bio_saksi2'),
					'ISI_COMPARATOR_CUSTOMER'=>$this->input->post('isi'),
					'KOMPARISI_PUSAT'=>$this->input->post('pusat'),
					'STATUS_COMPARATOR'=>$this->input->post('status'));
		// die(var_dump($data));
		$this->M_model->insert('0_1_detail_customer',$data);
		redirect(base_url('Customer/goDet/'.$this->input->post('idCus')));
	}

	public function deleteDetCus($id){
		$where = array('NO_ID_DETAIL_CUSTOMER'=>$id);
		$this -> M_model -> delete($where,'0_1_detail_customer');
		header('location:'.base_url('Customer'));
	}

	public function editDetailCus(){
		$data['detaile']=$this->M_model->selectwhere('0_1_detail_customer',array('NO_ID_DETAIL_CUSTOMER'=>$this->uri->segment(3)));
		$this->load->view('customer/e_e_customer',$data);
	}

	public function updateDetCus(){
		$where['NO_ID_DETAIL_CUSTOMER'] = $this->input->post('idDetCus');
		$data = array(
					'NAMA_COMPARATOR_CUSTOMER'=>$this->input->post('nm_pem'),
					'PENERIMA'=>$this->input->post('nm_penerima'),
					'PEMBERI'=>$this->input->post('nm_pemberi'),
					'ISTRI_PEMBERI'=>$this->input->post('istri_pemberi'),
					'SAKSI_SATU'=>$this->input->post('saksi1'),
					'SAKSI_DUA'=>$this->input->post('saksi2'),
					'NO_KTP_COMPARATOR_CUSTOMER'=>$this->input->post('ktp'),
					'ALAMAT_COMPARATOR_CUSTOMER'=>$this->input->post('alamat'),
					'TEMPAT_LAHIR'=>$this->input->post('tmp'),
					'TANGGAL_LAHIR'=>$this->input->post('tgl'),
					'KABUPATEN_COMPARATOR_CUSTOMER'=>$this->input->post('kab'),
					'KECAMATAN_COMPARATOR_CUSTOMER'=>$this->input->post('kec'),
					'DESA_KELURAHAN_COMPARATOR_CUSTOMER'=>$this->input->post('des'),
					'RT_COMPARATOR_CUSTOMER'=>$this->input->post('rt'),
					'RW_COMPARATOR_CUSTOMER'=>$this->input->post('rw'),
					'BIODATA_SAKSI_SATU'=>$this->input->post('bio_saksi1'),
					'BIODATA_SAKSI_DUA'=>$this->input->post('bio_saksi2'),
					'ISI_COMPARATOR_CUSTOMER'=>$this->input->post('isi'),
					'KOMPARISI_PUSAT'=>$this->input->post('pusat'),
					'STATUS_COMPARATOR'=>$this->input->post('status'));
		$this->M_model->update('0_1_detail_customer',$data,$where);
		redirect(base_url('Customer/goDet/').$this->input->post('idCus'));
	}

	public function detailOrderCus(){
		$data['pemilik']=$this->M_model->selectwhere('0_1_data_customer',array('NO_ID_CUSTOMER'=>$this->uri->segment(3)));
		$data['orderDetail']=$this->M_model->selectwhere('1_0_data_order_customer',array('NO_ID_CUSTOMER'=>$this->uri->segment(3)));
		$this->load->view('customer/list_detail_order',$data);
	}
	public function deleteDataCustomer($id){
		$where = array('NO_ID_ORDER_CUSTOMER'=>$id);
		$this -> M_model -> delete($where,'1_0_data_order_customer');
		header('location:'.base_url('Customer/detailOrderCus/').$this->input->post('id_Cus'));
	}
	public function updateDataCus(){
		$where['NO_ID_ORDER_CUSTOMER'] = $this->input->post('id_ordCus');
		$data = array(
					'NOMOR_SURAT_ORDER_CUSTOMER'=>$this->input->post('no_surat'),
					'TANGGAL_SURAT_ORDER_CUSTOMER'=>$this->input->post('tgl_surat'),
					'NOMOR_LAIN_ORDER_CUSTOMER'=>$this->input->post('no_cover_order'),
					'TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER'=>$this->input->post('tgl_cover_note'),
					'KETERANGAN_ORDER_CUSTOMER'=>$this->input->post('ket'));
		$this->M_model->update('1_0_data_order_customer',$data,$where);
		redirect(base_url('Customer/detailOrderCus/').$this->input->post('idCus'));
	}

	public function prosesTambahProsesOrder(){
		$data = array(
			'NO_ID_CUSTOMER' => $this->input->post('no_id_customer'),
			'NOMOR_SURAT_ORDER_CUSTOMER' => $this->input->post('no_surat_order_customer'),
			'TANGGAL_SURAT_ORDER_CUSTOMER' => $this->input->post('tgl_surat'),
			'NOMOR_LAIN_ORDER_CUSTOMER' => $this->input->post('no_cover_note_notaris'),
			'TANGGAL_NOMOR_LAIN_ORDER_CUSTOMER' => $this->input->post('tgl_cover_note_notaris'),
			'KETERANGAN_ORDER_CUSTOMER' => $this->input->post('ket_order')
		);
		$this->M_model->insert('1_0_data_order_customer', $data);
		return redirect(base_url('Customer/detailOrderCus/').$this->input->post('no_id_customer'));
	}

	public function detailRincianOrderCus(){
		$id_order_customer = $this->uri->segment(3);
		$id_customer = $this->uri->segment(4);
		$data_cust = $this->db->get_where('0_1_data_customer',array('NO_ID_CUSTOMER'=>$id_customer))->row_array();
		// $no_id_order_customer = $this->input->post('no_id_order_customer');
		// $no_id_customer = $this->input->post('no_id_customer');
		$data['saksi'] = $this->M_model->select('0_2_data_saksi');
		$data['jenisOrder'] = $this->M_model->selectwhere('0_0_jenis_order',array('JENIS_CUSTOMER'=>$data_cust['JENIS_CUSTOMER']));	
		$data['customer'] = $this->M_model->selectwhere('0_1_data_customer', array('NO_ID_CUSTOMER '=>$id_customer));
		$data['rincian'] = $this->M_model->select('0_1_data_customer', array('NO_ID_CUSTOMER' => $id_customer));
		$data['rincian1'] = $this->M_model->selectwhere('1_0_data_order_customer', array('NO_ID_ORDER_CUSTOMER' => $id_order_customer));
		$data['rincian2'] = $this->M_model->selectwhere('1_1_detail_order_customer', array('NO_ID_ORDER_CUSTOMER'=>$id_order_customer));
		$this->load->view('customer/list_rincian_detail_order', $data);
	}
	public function prosesTambahDataOrderCus(){
		$data = array(
			'NO_ID_ORDER_CUSTOMER' => $this->input->post('id_OrdCus'),
			'NO_ID_JENISORDER' => $this->input->post('jenis_ord'),
			'DETAIL_ORDER_ATAS_NAMA' => $this->input->post('order_cus_atas_nama'),
			'PASANGAN_DETAIL_ORDER'=>$this->input->post('pas_order_cus_atas_nama'),
			'BIODATA_PASANGAN_DETAIL_ORDER'=>$this->input->post('bio_pas_order_cus_atas_nama'),
			'SAKSI_SATU'=>$this->input->post('saksi1'),
			'BIODATA_SAKSI_SATU'=>$this->input->post('bio_saksi1'),
			'SAKSI_DUA'=>$this->input->post('saksi2'),
			'BIODATA_SAKSI_DUA'=>$this->input->post('bio_saksi2'),
			'NOMINAL_DETAIL_ORDER' => $this->input->post('nominal_pengikatan'),
			'KETERANGAN_DETAIL_ORDER' => $this->input->post('ket')
		);
		$this->M_model->insert('1_1_detail_order_customer', $data);
		return redirect(base_url('Customer/detailRincianOrderCus/').$this->input->post('id_OrdCus').'/'.$this->input->post('id_Cus'));
	}
	public function updateDatOrderCus(){
		$where['NO_ID_DETAIL_ORDER'] = $this->input->post('id_DetOrd');
		$data = array(
					'NO_ID_ORDER_CUSTOMER'=>$this->input->post('no_id_ordCus'),
					'NO_ID_JENISORDER'=>$this->input->post('jenis_ord'),
					'DETAIL_ORDER_ATAS_NAMA'=>$this->input->post('order_cus_atas_nama'),
					'PASANGAN_DETAIL_ORDER'=>$this->input->post('pas_order_cus_atas_nama'),
					'BIODATA_PASANGAN_DETAIL_ORDER'=>$this->input->post('bio_pas_order_cus_atas_nama'),
					'SAKSI_SATU'=>$this->input->post('saksi1'),
					'BIODATA_SAKSI_SATU'=>$this->input->post('bio_saksi1'),
					'SAKSI_DUA'=>$this->input->post('saksi2'),
					'BIODATA_SAKSI_DUA'=>$this->input->post('bio_saksi2'),
					'NOMINAL_DETAIL_ORDER'=>$this->input->post('nominal_pengikatan'),
					'KETERANGAN_DETAIL_ORDER'=>$this->input->post('ket'));
		$this->M_model->update('1_1_detail_order_customer',$data,$where);
		redirect(base_url('Customer/detailRincianOrderCus/').$this->input->post('id_OrdCus'));
	}
	public function deleteDetailOrderCustomer($id){
		$where = array('NO_ID_DETAIL_ORDER'=>$id);
		$this->M_model-> delete($where,'1_1_detail_order_customer');
		header('location:'.base_url('Customer/detailOrderCus/').$this->input->post('id_Cus'));
	}

	// Controller List Detail Obyek Order Customer
	public function detailObyekOrderCus(){
		$id_detail_order = $this->uri->segment(3);
		$id_order_customer = $this->uri->segment(4);
		$id_customer = $this->uri->segment(5);
		$data['rincian2'] = $this->M_model->selectwhere('1_1_detail_order_customer', array('NO_ID_ORDER_CUSTOMER'=>$id_order_customer));
		$data['obyek'] = $this->M_model->selectwhere('1_1_detail_order_customer', array('NO_ID_DETAIL_ORDER' => $id_detail_order));
		$data['obyek1'] = $this->M_model->selectwhere('1_0_data_order_customer', array('NO_ID_ORDER_CUSTOMER' => $id_order_customer));
		$data['obyek2'] = $this->M_model->selectwhere('1_2_detail_obyek_order_customer', array('NO_ID_DETAIL_ORDER '=>$id_detail_order));
		$data['customer'] = $this->M_model->selectwhere('0_1_data_customer', array('NO_ID_CUSTOMER '=>$id_customer));
		$data['proses_order'] = $this->M_model->select('2_0_proses_order_customer');
		$this->load->view('customer/list_detail_obyek_order_customer', $data);
	}

	public function prosesTambahListDetOrdCus(){
		$data = array('NO_ID_DETAIL_ORDER' => $this->input->post('id_detOrd'),
						 'NO_ID_JENISORDER' => $this->input->post('id_jnsOrd'),
						 'JENIS_OBYEK' => $this->input->post('jenis_oby'),
						 'NOMOR_OBYEK' => $this->input->post('nomor_oby'),
						 'TANGGAL_OBYEK' => $this->input->post('tgl_oby'),
						 'JENIS_IDENTIFIKASI_OBYEK' => $this->input->post('jns_iden_oby'),
						 'NOMOR_IDENTIFIKASI_OBYEK' => $this->input->post('nmr_iden_oby'),
						 'TANGGAL_IDENTIFIKASI_OBYEK' => $this->input->post('tgl_iden_oby'),
						 'ATAS_NAMA_OBYEK' => $this->input->post('ord_atas_nama'),
						 'LUAS_OBYEK' => $this->input->post('luas_oby_diikat'),
						 'NOMOR_OBYEK_PAJAK' => $this->input->post('nop'),
						 'KETERANGAN_LAIN_OBYEK' => $this->input->post('ket'),
						 'NOMINAL_PARTIAL_OBYEK' => $this->input->post('parsial'));
		$this->M_model->insert('1_2_detail_obyek_order_customer',$data);
		redirect(base_url('Customer/detailObyekOrderCus/').$this->input->post('id_detOrd').'/'.$this->input->post('id_OrdCus').'/'.$this->input->post('id_Cus'));
	}
	public function UpdateListDetOrdCus(){
		$where['NO_ID_OBYEK_ORDER_CUSTOMER']= $this->input->post('id_ObyOrdCus');
		$data = array(	 'NO_ID_DETAIL_ORDER' => $this->input->post('id_detOrd'),
						 'NO_ID_JENISORDER' => $this->input->post('id_jnsOrd'),
						 'JENIS_OBYEK' => $this->input->post('jenis_oby'),
						 'NOMOR_OBYEK' => $this->input->post('nomor_oby'),
						 'TANGGAL_OBYEK' => $this->input->post('tgl_oby'),
						 'JENIS_IDENTIFIKASI_OBYEK' => $this->input->post('jns_iden_oby'),
						 'NOMOR_IDENTIFIKASI_OBYEK' => $this->input->post('nmr_iden_oby'),
						 'TANGGAL_IDENTIFIKASI_OBYEK' => $this->input->post('tgl_iden_oby'),
						 'ATAS_NAMA_OBYEK' => $this->input->post('ord_atas_nama'),
						 'LUAS_OBYEK' => $this->input->post('luas_oby_diikat'),
						 'NOMOR_OBYEK_PAJAK' => $this->input->post('nop'),
						 'KETERANGAN_LAIN_OBYEK' => $this->input->post('ket'),
						 'NOMINAL_PARTIAL_OBYEK' => $this->input->post('parsial'));
		$this->M_model->update('1_2_detail_obyek_order_customer',$data,$where);
		redirect(base_url('Customer/detailObyekOrderCus/').$this->input->post('id_detOrd').'/'.$this->input->post('id_OrdCus').'/'.$this->input->post('id_Cus'));
	}
	public function deletelistdetail($id){
		$where = array('NO_ID_OBYEK_ORDER_CUSTOMER'=>$id);
		$this ->M_model-> delete($where,'1_2_detail_obyek_order_customer');
		redirect(base_url('Customer/detailObyekOrderCus/').$this->input->post('id_detOrd').'/'.$this->input->post('id_OrdCus').'/'.$this->input->post('id_Cus'));
	}


	public function inputProsesPengikatan(){
		$id_obyek_order_customer = $this->uri->segment(3);
		$id_jenisorder = $this->uri->segment(4);
		$id_order_customer = $this->uri->segment(5);
		$id_detail_order = $this->uri->segment(6);
		$data['proses_order'] = $this->M_model->selectwhere('0_0_proses_order', array('NO_ID_JENIS_ORDER'=>$id_jenisorder));
		$data['detail_obyek_order_customer'] = $this->M_model->selectwhere('1_2_detail_obyek_order_customer', array('NO_ID_OBYEK_ORDER_CUSTOMER'=>$id_obyek_order_customer));
		$data['data_order_customer'] = $this->M_model->selectwhere('1_0_data_order_customer', array('NO_ID_ORDER_CUSTOMER'=>$id_order_customer));
		$data['detail_order_customer'] = $this->M_model->selectwhere('1_1_detail_order_customer', array('NO_ID_DETAIL_ORDER'=>$id_detail_order));
		// $data['pengikatan'] = $this->M_model->prosesPengikatan($id);
		$this->load->view('customer/i_proses_pengikatan', $data);
	}
	public function dokumen(){
		$id_detail_order = $this->uri->segment(3);
		// $id_order_customer = $this->uri->segment(4);
		// $id_customer = $this->uri->segment(5);
		$data['detail_order'] = $this->M_model->selectwhere('1_1_detail_order_customer', array('NO_ID_DETAIL_ORDER'=>$id_detail_order));
		// $data['customer'] = $this->M_model->selectwhere('0_1_data_customer', array('NO_ID_CUSTOMER '=>$id_customer));
		$data['dokumen'] = $this->M_model->selectwhere('1_3_dokumen', array('NO_ID_DETAIL_ORDER' =>$id_detail_order));
		$this->load->view('customer/v_dokumen',$data);
	}
	public function t_dokumen(){
		$config['upload_path']          = './gallery/dokumen/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		// $config['max_size']             = 500000;
		// $config['max_width']            = 500000;
		// $config['max_height']           = 300000;

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('gambar_dok'))
		{
			$upload_data = $this->upload->data();
			$id_detail_order = $this->input->post('id_detOrd');
			$jenis_dokumen = $this->input->post('jns_dok');
			$foto = "gallery/dokumen/".$upload_data['file_name'];
		$data = array(
				'NO_ID_DETAIL_ORDER' => $id_detail_order,
				'JENIS_DOKUMEN_UPLOAD' => $jenis_dokumen,
				'FILE_NAME_GAMBAR' => $foto
			);
		$this->M_model->insert('1_3_dokumen',$data);
		redirect(base_url('Customer/dokumen/').$this->input->post('id_detOrd'));
		}else{
			echo "gagal";
		}
	}
	public function edit_dokumen(){
		$config['upload_path']          = './gallery/dokumen/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 500000;
		$config['max_width']            = 500000;
		$config['max_height']           = 300000;

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('gambar_dok'))
		{
			$upload_data = $this->upload->data();
			$where['NO_ID_DOKUMEN']= $this->input->post('id_dok');
			$id_detail_order = $this->input->post('id_detOrd');
			$jenis_dokumen = $this->input->post('jns_dok');
			$foto = "gallery/dokumen/".$upload_data['file_name'];
		$data = array(
				'NO_ID_DETAIL_ORDER' => $id_detail_order,
				'JENIS_DOKUMEN_UPLOAD' => $jenis_dokumen,
				'FILE_NAME_GAMBAR' => $foto
			);
		$rt = $this->input->post('id_detOrd');
		$this->M_model->update('1_3_dokumen',$data,$where);
		redirect(base_url('Customer/dokumen/'.$rt));
		}
		echo "gagal";
	}

	public function listProsesPengikatanObyek(){
		$id = $this->uri->segment(3);
		$data['pengikatan'] = $this->M_model->prosesPengikatan($id);
		$this->load->view('customer/i_proses_pengikatan', $data);
	}

	public function updateComPusat(){
		$data = array('NAMA_COMPARATOR_CUSTOMER' =>$this->input->post('nama') ,
					'KOMPARISI_PUSAT'=>$this->input->post('kompa'));
		$this->M_model->update('0_1_detail_customer',$data,array('NO_ID_DETAIL_CUSTOMER'=>$this->input->post('idDet')));
		redirect(base_url('Customer/goDet/'.$this->input->post('idDet')));
	}


	public function cetakAPHT(){
		$data['ApHT'] = $this->M_proses->cetakApht();
		$this->load->view('APHT', $data);
	}

	public function cetakAPHTbyUser(){
		$view = $this->M_proses->cetakAphtbyUser();
		$NOMOR_AKTA = $view[0]->NOMOR_AKTA;
		$TGL_AKTA = $view[0]->TGL_AKTA;
		$PASANGAN_DETAIL_ORDER = $view[0]->PASANGAN_DETAIL_ORDER;
		$BIODATA_PASANGAN_DETAIL_ORDER = $view[0]->BIODATA_PASANGAN_DETAIL_ORDER;
		$NOMOR_SURAT_ORDER_CUSTOMER = $view[0]->NOMOR_SURAT_ORDER_CUSTOMER;
		$DETAIL_ORDER_ATAS_NAMA = $view[0]->DETAIL_ORDER_ATAS_NAMA;
		// $KOMPARISI_PUSAT = $view[0]->KOMPARISI_PUSAT;
		$DETAIL_ORDER_ATAS_NAMA = $view[0]->DETAIL_ORDER_ATAS_NAMA;
		$TANGGAL_OBYEK = $view[0]->TANGGAL_OBYEK;
		$NOMOR_OBYEK = $view[0]->NOMOR_OBYEK;
		$NOMINAL_DETAIL_ORDER = $view[0]->NOMINAL_DETAIL_ORDER;
		$NOMINAL_PARTIAL_OBYEK = $view[0]->NOMINAL_PARTIAL_OBYEK;
		$TANGGAL_IDENTIFIKASI_OBYEK = $view[0]->TANGGAL_IDENTIFIKASI_OBYEK;
		$LUAS_OBYEK = $view[0]->LUAS_OBYEK;
		$DETAIL_ORDER_ATAS_NAMA = $view[0]->DETAIL_ORDER_ATAS_NAMA;
		$NOMOR_IDENTIFIKASI_OBYEK = $view[0]->NOMOR_IDENTIFIKASI_OBYEK;
		$NOMOR_OBYEK_PAJAK = $view[0]->NOMOR_OBYEK_PAJAK;
		// $NAMA_CUSTOMER = $view[0]->NAMA_CUSTOMER;
		$SAKSI_SATU = $view[0]->SAKSI_SATU;
		$BIODATA_SAKSI_SATU = $view[0]->BIODATA_SAKSI_SATU;
		$SAKSI_DUA = $view[0]->SAKSI_DUA;
		$BIODATA_SAKSI_DUA = $view[0]->BIODATA_SAKSI_DUA;

		$document = file_get_contents(base_url().'dokumen/lampiran/APHT.rtf');
		$document = str_replace('#NOMOR_AKTA', $NOMOR_AKTA, $document);
		$document = str_replace('#TGL_AKTA', $TGL_AKTA, $document);
		$document = str_replace('#PASANGAN_DETAIL_ORDER', $PASANGAN_DETAIL_ORDER, $document);
		$document = str_replace('#BIODATA_PASANGAN_DETAIL_ORDER', $BIODATA_PASANGAN_DETAIL_ORDER, $document);
		$document = str_replace('#NOMOR_SURAT_ORDER_CUSTOMER', $NOMOR_SURAT_ORDER_CUSTOMER, $document);
		$document = str_replace('#DETAIL_ORDER_ATAS_NAMA', $DETAIL_ORDER_ATAS_NAMA, $document);
		$document = str_replace('#DETAIL_ORDER_ATAS_NAMA', $DETAIL_ORDER_ATAS_NAMA, $document);
		$document = str_replace('#TANGGAL_OBYEK', $TANGGAL_OBYEK, $document);
		$document = str_replace('#NOMOR_OBYEK', $NOMOR_OBYEK, $document);
		$document = str_replace('#NOMINAL_DETAIL_ORDER', $NOMINAL_DETAIL_ORDER, $document);
		$document = str_replace('#NOMINAL_PARTIAL_OBYEK', $NOMINAL_PARTIAL_OBYEK, $document);
		$document = str_replace('#TANGGAL_IDENTIFIKASI_OBYEK', $TANGGAL_IDENTIFIKASI_OBYEK, $document);
		$document = str_replace('#LUAS_OBYEK', $LUAS_OBYEK, $document);
		$document = str_replace('#DETAIL_ORDER_ATAS_NAMA', $DETAIL_ORDER_ATAS_NAMA, $document);
		$document = str_replace('#NOMOR_IDENTIFIKASI_OBYEK', $NOMOR_IDENTIFIKASI_OBYEK, $document);
		$document = str_replace('#NOMOR_OBYEK_PAJAK', $NOMOR_OBYEK_PAJAK, $document);
		$document = str_replace('#SAKSI_SATU', $SAKSI_SATU, $document);
		$document = str_replace('#BIODATA_SAKSI_SATU', $BIODATA_SAKSI_SATU, $document);
		$document = str_replace('#SAKSI_DUA', $SAKSI_DUA, $document);
		$document = str_replace('#BIODATA_SAKSI_DUA', $BIODATA_SAKSI_DUA, $document);
		header("Content-type: application/msword");
		header("Content-disposition: inline; filename=apht.docx");
		header("Content-length: ".strlen($document));
		echo $document;
		// echo json_encode($DETAIL_ORDER_ATAS_NAMA);
	}

	public function cetakSKMHT(){
		$data['SKMHT'] = $this->M_proses->cetakSKMHT();
		$this->load->view('SKMHT', $data);
	}

	public function cetakSKMHTbyUser(){
		$data['SKMHT'] = $this->M_proses->cetakSKMHTbyUser();
		$this->load->view('SKMHT', $data);
	}

	public function cetakHIBAH(){
		$data['HIBAH'] = $this->M_proses->cetakHIBAH();
		$this->load->view('HIBAH', $data);
	}

	public function cetakHIBAHbyUser(){
		$data['HIBAH'] = $this->M_proses->cetakHIBAHbyUser();
		$this->load->view('HIBAH', $data);
	}
}
