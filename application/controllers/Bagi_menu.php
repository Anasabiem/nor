<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bagi_menu extends CI_Controller {
	function __construct(){
			parent::__construct();
			$this->load->model('M_proses');
		}

	public function index(){
		$data['user']= $this->db->get_where('0_user',array('TYPE_USER'=>2))->result();
		$data['cust'] = $this->db->get('0_1_data_customer')->result();
		$data['pembagian'] = $this->M_model->get_pembagian()->result(); 
		$this->load->view('bagi_menu',$data);
	}
	public function save_cetak(){
		$nama_karyawan=$this->input->post('nm_karyawan');
		$pembagian_karywancetak=$this->input->post('Pembagian_cetak');
		for ($i=0; $i < count($pembagian_karywancetak) ; $i++) 
		{ 
			$this->M_model->insert('0_3_user_cust',array('NO_ID_USR' =>$nama_karyawan, 
									'NO_ID_CUST'=>$pembagian_karywancetak[$i]));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
}
