<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_order extends CI_Controller {

	public function index(){
		$data['ord']=$this->M_model->select('0_0_jenis_order')->result();
		$this->load->view('order/v_jenis_order',$data);
	}

	public function tambah(){
		$this->load->view('order/i_jenis_order');
	}

	public function tambahJenisOrder(){
		$data = array(	'NAMA_JENIS_ORDER' => $this->input->post('jns_order'),
						'OBYEK_JENIS_ORDER'=>$this->input->post('oby_order'),
						'ASAL_OBYEK_JENIS_ORDER'=> $this->input->post('asl_obyek'),
						'KETERANGAN_JENIS_ORDER'=>$this->input->post('ket'));
		$this->M_model->insert('0_0_jenis_order',$data);
		redirect(base_url('Jenis_order'));
	}

	public function hapusJenisOrder($id){
		$where = array('NO_ID_JENISORDER'=>$id);
		$this -> M_model -> delete($where,'0_0_jenis_order');
		header('location:'.base_url('Jenis_order'));
	}

	public function editJenisOrder(){
		$id = $this->uri->segment(3);
		$data['dataJns_order']=$this->M_model->selectwhere('0_0_jenis_order',array('NO_ID_JENISORDER'=>$id))->result();
		$this->load->view('order/e_jenis_order',$data);
	}

	public function updateJenisOrder(){
		$where = array(	'NO_ID_JENISORDER'=>$this->input->post('id_jns_order'));
		$data = array(	'NAMA_JENIS_ORDER' => $this->input->post('jns_order'),
						'OBYEK_JENIS_ORDER'=>$this->input->post('oby_order'),
						'ASAL_OBYEK_JENIS_ORDER'=> $this->input->post('asl_obyek'),
						'KETERANGAN_JENIS_ORDER'=>$this->input->post('ket'));
		// die(var_dump($data));
		$this->M_model->update('0_0_jenis_order',$data,$where);
		redirect(base_url('order/Jenis_order/proses_order'));
	}

	public function proses_order(){
		$id = $this->uri->segment(3);
		$data['pros_ord']=$this->M_model->selectwhere('0_0_proses_order',array('NO_ID_JENIS_ORDER'=>$id))->result();
		$data['id_jenis']=$id;
		$this->load->view('order/v_data_proses_order',$data);
	}

	public function tambah_Proses_order(){
		$id = $this->uri->segment(3);
		$data['jns_ord']=$this->M_model->selectwhere('0_0_jenis_order',array('NO_ID_JENISORDER'=>$id))->result();
		//$data['t_ord']=$this->M_model->selectwhere('0_0_proses_order',array('NO_ID_JENIS_ORDER'=>$id))->result();
		$this->load->view('order/i_proses_order', $data);
	}

	public function prosesTambahOrder(){
		$data['NO_ID_JENIS_ORDER'] = $this->input->post('jns_order');
		$data['NO_ID_PROSES_ORDER'] = $this->input->post('ket_order');
		$data['NAMA_PROSES_ORDER'] = $this->input->post('nm_proses_order');
		$data['POST_PROSES_ORDER'] = $this->input->post('pos_proses_order');
		$data['KETERANGAN_PROSES_ORDER'] = $this->input->post('ket');
		$this->M_model->insert('0_0_proses_order', $data);
		return redirect(base_url('Jenis_order/'));
	}

	public function editProsesOrder(){
		$id = $this->uri->segment(3);
		// $data['e_ord']=$this->M_model->selectwhere('0_0_jenis_order',array('NO_ID_JENISORDER'=>$id))->result();
		$data['e_ord2']=$this->M_model->selectwhere('0_0_proses_order',array('NO_ID_PROSES_ORDER'=>$id))->result();
		//$data['t_ord']=$this->M_model->selectwhere('0_0_proses_order',array('NO_ID_JENIS_ORDER'=>$id))->result();
		$this->load->view('order/e_proses_order',$data);
	}
	public function updateProsesOrder(){
		$where = array(	'NO_ID_PROSES_ORDER'=>$this->input->post('id_pros'));
		$data = array(	'NAMA_PROSES_ORDER' => $this->input->post('nm_pros'),
						'POST_PROSES_ORDER'=>$this->input->post('pos_pros'),
						'KETERANGAN_PROSES_ORDER'=>$this->input->post('ket'));
		// die(var_dump($data));
		$this->M_model->update('0_0_proses_order',$data,$where);
		redirect(base_url('Jenis_order/proses_order'));
	}

	public function prosesEditOrder(){
		$data = array(
			'NO_ID_JENIS_ORDER' => $this->input->post('id_jns_order'),
			'NO_ID_PROSES_ORDER' => $this->input->post('id_pros'),
			'NAMA_PROSES_ORDER' => $this->input->post('nm_pros'),
			'POST_PROSES_ORDER' => $this->input->post('pos_pros'),
			'KETERANGAN_PROSES_ORDER' => $this->input->post('ket')
		);
		// die(var_dump($data));
		$this->M_model->update('0_0_proses_order',$data,array('NO_ID_PROSES_ORDER'=>$this->input->post('id_pros')));
		return redirect(base_url('Jenis_order/'));
	}

	public function hapusProsesOrder(){
		$id_proses_order = $this->uri->segment(3);
		$id_jenis_order = 0000000001;
		$this->M_model->delete(array('NO_ID_PROSES_ORDER'=>$id_proses_order, 'NO_ID_JENIS_ORDER'=>$id_jenis_order), '0_0_proses_order');
		return redirect(base_url('Jenis_order/proses_order/0000000001'));
	}
}
