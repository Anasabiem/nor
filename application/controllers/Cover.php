<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cover extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('Ciqrcode');
		$this->load->library('Zend');
	}
	public function skmht()
	{
		$this->load->view('qrcode/qr_skmht');
	}
	public function apht()
	{
		$this->load->view('qrcode/qr_apht');
	}
	public function akta()
	{
		$this->load->view('qrcode/qr_akta');
	}
	public function hibah()
	{
		$this->load->view('qrcode/qr_hibah');
	}

	public function qr($kodenya){
		QRcode::png(
			$kodenya,
			$outfile = false,
			$level = QR_ECLEVEL_H,
			$size = 5,
			$margin = 2

		);
	}
	public function barcode($kodenya){
		$this->zend->load('Zend/Barcode');
		Zend_barcode::render('code128','image',array('text'=>$kodenya));
	}
}
