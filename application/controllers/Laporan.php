<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function index(){
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		if ($date_from == NULL || $date_to == NULL) {
			$data['laporan'] = NULL;
			$data['date_from'] = NULL;
			$data['date_to'] = NULL;
			$this->load->view('laporan/laporan', $data);
		}else {
			$data['date_from'] = $date_from;
			$data['date_to'] = $date_to;
			$data['laporan'] = $this->M_model->laporan($date_from, $date_to);
			$this->load->view('laporan/laporan', $data);
		}
	}
}
