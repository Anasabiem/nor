<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProsesPengikatan extends CI_Controller {
	function __construct(){
			parent::__construct();
			$this->load->model('M_proses');
		}

	public function index(){

		$data['data']=$this->M_proses->proses_customer()->result();
		$this->load->view('proses/isi_proses',$data);
	}
	public function load_index(){
		$data['isi'] = $this->M_proses->get_proses_customer()->result();
		$this->load->view('proses/v_proses',$data);
	}

	public function List_proses(){
		// $get_count_harga = $this->db->get
		$data['cus']=$this->M_proses->get_list_harga_proses_admin()->result();
		// die(var_dump($data['cus']));
		$this->load->view('customer/v_list_proses',$data);
	}

	public function proses_list(){
    $NO_ID_OBYEK_ORDER_CUSTOMER = $this->input->post('NO_ID_OBYEK_ORDER_CUSTOMER');
    $NO_ID_DETAIL_ORDER = $this->input->post('NO_ID_DETAIL_ORDER');
    $NO_ID_JENISORDER = $this->input->post('NO_ID_JENISORDER');
    $NO_ID_ORDER_CUSTOMER = $this->input->post('NO_ID_ORDER_CUSTOMER');
    $NO_ID_CUSTOMER = $this->input->post('NO_ID_CUSTOMER');
    $NAMA_PROSES_ORDER = $this->input->post('NAMA_PROSES_ORDER');
    $NO_ID_PROSES_ORDER = $this->input->post('NO_ID_PROSES_ORDER');
    $NOMINAL_BIAYA_PROSES = $this->input->post('perCost');
    for ($i=0; $i < sizeof($NO_ID_OBYEK_ORDER_CUSTOMER); $i++) {
      $data = array(
        'NO_ID_OBYEK_ORDER_CUSTOMER'=>$NO_ID_OBYEK_ORDER_CUSTOMER[$i],
        'NO_ID_JENISORDER'=>$NO_ID_JENISORDER,
        'NO_ID_PROSES_ORDER'=>$NO_ID_PROSES_ORDER[$i],
        'NAMA_PROSES_ORDER'=>$NAMA_PROSES_ORDER[$i],
        'NOMINAL_BIAYA_PROSES'=>$NOMINAL_BIAYA_PROSES[$i]
      );
      $this->M_model->insert('2_0_proses_order_customer', $data);
    }
    return redirect(base_url('prosesPengikatan/listProsesPengikatanObyek/')
    .$NO_ID_OBYEK_ORDER_CUSTOMER[0].'/'
		.$NO_ID_ORDER_CUSTOMER.'/'
		.$NO_ID_CUSTOMER);
	}

  public function listProsesPengikatanObyek(){
    $NO_ID_OBYEK_ORDER_CUSTOMER = $this->uri->segment(3);
    $NO_ID_ORDER_CUSTOMER = $this->uri->segment(4);
    $NO_ID_CUSTOMER = $this->uri->segment(5);
    $data['proses_order_customer'] = $this->M_model->selectwhere('2_0_proses_order_customer', array('NO_ID_OBYEK_ORDER_CUSTOMER'=>$NO_ID_OBYEK_ORDER_CUSTOMER));
    $data['data_customer'] = $this->M_model->selectwhere('0_1_data_customer', array('NO_ID_CUSTOMER'=>$NO_ID_CUSTOMER));
    $data['data_order_customer'] = $this->M_model->selectwhere('1_0_data_order_customer', array('NO_ID_ORDER_CUSTOMER'=>$NO_ID_ORDER_CUSTOMER));
		$data['detail_order_customer'] = $this->M_model->selectwhere('1_1_detail_order_customer', array('NO_ID_ORDER_CUSTOMER'=>$NO_ID_ORDER_CUSTOMER));
    $this->load->view('customer/list_proses_pengikatan_obyek', $data);
  }

	public function prosesTambahListDetOrdCus(){
		$data = array('NO_ID_DETAIL_ORDER' => $this->input->post('id_detOrd'),
						 'NO_ID_JENISORDER' => $this->input->post('id_jnsOrd'),
						 'JENIS_OBYEK' => $this->input->post('jenis_oby'),
						 'NOMOR_OBYEK' => $this->input->post('nomor_oby'),
						 'TANGGAL_OBYEK' => $this->input->post('tgl_oby'),
						 'JENIS_IDENTIFIKASI_OBYEK' => $this->input->post('jns_iden_oby'),
						 'NOMOR_IDENTIFIKASI_OBYEK' => $this->input->post('nmr_iden_oby'),
						 'TANGGAL_IDENTIFIKASI_OBYEK' => $this->input->post('tgl_iden_oby'),
						 'ATAS_NAMA_OBYEK' => $this->input->post('ord_atas_nama'),
						 'LUAS_OBYEK' => $this->input->post('luas_oby_diikat'),
						 'NOMOR_OBYEK_PAJAK' => $this->input->post('nop'),
						 'KETERANGAN_LAIN_OBYEK' => $this->input->post('ket'),
						 'NOMINAL_PARTIAL_OBYEK' => $this->input->post('parsial'));
		$this->M_model->insert('1_2_detail_obyek_order_customer',$data);
		redirect(base_url('Customer/detailObyekOrderCus/').$this->input->post('id_detOrd').'/'.$this->input->post('id_order_customer').'/'.$this->input->post('id_customer'));
	}

	public function editBiayaUtkProses(){
		$no = $this->uri->segment(3);
		$data = array(
			'NOMINAL_BIAYA_PROSES'=>$this->input->post('biaya'.$no)
		);
		$where = array(
			'NO_ID_PROSES_ORDER_CUSTOMER'=>$this->input->post('NO_ID_PROSES_ORDER_CUSTOMER'.$no)
		);
		$id_customer = $this->input->post('id_customer'.$no);
		$id_order_customer = $this->input->post('id_order_customer'.$no);
		$this->M_model->update('2_0_proses_order_customer', $data, $where);
		return redirect(base_url('ProsesPengikatan/listProsesPengikatanObyek/').$this->input->post('NO_ID_OBYEK_ORDER_CUSTOMER'.$no).'/'.$id_order_customer.'/'.$id_customer);
	}

	public function editStatusUtkProses(){
		$no = $this->uri->segment(3);
		$id_customer = $this->input->post('id_customer'.$no);
		$id_order_customer = $this->input->post('id_order_customer'.$no);
		$data = array(
			'STATUS_PROSES_ORDER_CUSTOMER'=>$this->input->post('status'.$no),
			'TGLSTTS_PROSES_ORDER_CUSTOMER'=>$this->input->post('tgl_status'.$no)
		);
		$where = array(
			'NO_ID_PROSES_ORDER_CUSTOMER'=>$this->input->post('id_ProsOrdCus'.$no)
		);
		$this->M_model->update('2_0_proses_order_customer', $data, $where);
		return redirect(base_url('ProsesPengikatan/listProsesPengikatanObyek/').$this->input->post('id_obyek_order_customer'.$no).'/'.$id_order_customer.'/'.$id_customer);
	}

	public function editStatusUtkPegawai(){
		$no = $this->uri->segment(3);
		$id_customer = $this->input->post('id_customer'.$no);
		$id_order_customer = $this->input->post('id_order_customer'.$no);
		$data = array(
			'ID_USER'=>$this->input->post('pegawai'.$no)
		);
		$where = array(
			'NO_ID_PROSES_ORDER_CUSTOMER'=>$this->input->post('id_ProsOrdCus'.$no)
		);
		$this->M_model->update('2_0_proses_order_customer', $data, $where);
		return redirect(base_url('ProsesPengikatan/listProsesPengikatanObyek/').$this->input->post('id_obyek_order_customer'.$no).'/'.$id_order_customer.'/'.$id_customer);
	}
	public function update(){
		$where['NO_ID_PROSES_ORDER_CUSTOMER'] = $this->input->post('id_obyOrdCus');
		$data = array(
					'NOMOR_AKTA'=>$this->input->post('no_akt'),
					'TGL_AKTA'=>$this->input->post('tgl_akt'),
					'JAM'=>$this->input->post('jam_akt'));
		$this->M_model->update('2_0_proses_order_customer',$data,$where);
		redirect(base_url('ProsesPengikatan'));
	}

	public function save_akta(){
		$id = $this->uri->segment(3);
		$name = 'tgl_akta'.$id;
		// echo json_encode($name);
		$tgl = $this->input->post($name);
		$url = $this->session->userdata('current_url');
		// echo json_encode($url);
		$this->M_model->update('2_0_proses_order_customer', array('TGL_AKTA'=>$tgl), array('NO_ID_PROSES_ORDER_CUSTOMER'=>$id));
		return redirect($url);
	}
}
