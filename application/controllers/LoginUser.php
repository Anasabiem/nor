<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginUser extends CI_Controller {

	public function index()
	{
		$this->load->view('v_login');
	}
	public function logindulu(){
		$where = array('LOGIN_USER' =>$this->input->post('usernm'),
						'PSWD_USER'=>md5($this->input->post('psw')),
						'STATUS_LOGIN'=>0,
						'STATUS_USER'=>'AKTIF');
		$cek = $this->M_model->cek_login("0_user",$where)->num_rows();
		$cek1 = $this->M_model->cek_login("0_user",$where)->row();
		if($cek > 0 && $cek->ID_USER!='00017'){
			$data_session = array(
				'id' => $cek1->ID_USER,
				'status' => "login",
				'tipene' =>$cek1->TYPE_USER,
				);

			$this->session->set_userdata($data_session);
			$this->db->update('0_user',array('STATUS_LOGIN' => 1 ),array('ID_USER'=>$cek1->ID_USER));
			redirect(base_url("Home"));
		}elseif ($cek > 0 && $cek->ID_USER=='00017') {
			$data_session = array(
				'id' => $cek1->ID_USER,
				'status' => "login",
				'tipene' =>$cek1->TYPE_USER,
				);
				redirect(base_url("Home"));
			$this->session->set_userdata($data_session);
		}else{
			redirect(base_url("LoginUser"));
		}
	}
	function logout(){
		$this->session->sess_destroy();
		$this->db->update('0_user',array('STATUS_LOGIN' => 0 ),array('ID_USER'=>$this->session->userdata('id')));
		redirect(base_url('LoginUser'));
	}
}
