<?php

class Cetak extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
        }

        public function index()
        {
                $this->load->view('trial_error/cetak');
        }

        public function cetak(){
          $nama = $this->input->post('nama');
          $document = file_get_contents(base_url().'dokumen/lampiran/percobaan.rtf');
          $document = str_replace('#nama', $nama, $document);
          header("Content-type: application/msword");
          header("Content-disposition: inline; filename=suratsakral.docx");
          header("Content-length: ".strlen($document));
          echo $document;
        }

}
?>
